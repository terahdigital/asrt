FROM php:7.3-cli-stretch

COPY ./ /src
RUN apt-get update -y \
    && apt-get install -y build-essential git  libzip-dev zip \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install -j$(nproc) zip
RUN git clone git://github.com/phalcon/php-zephir-parser.git \
    && cd php-zephir-parser \
    && /usr/local/bin/phpize \
    && ./configure --with-php-config=/usr/local/bin/php-config \
    && make \
    && make install \
    && pecl install xdebug psr \
    && docker-php-ext-enable zephir_parser xdebug psr
    && curl -L https://github.com/phalcon/zephir/releases/download/0.12.17/zephir.phar --output /usr/local/bin/zephir \
    && chmod +x /usr/local/bin/zephir
RUN cd /src/src/terah/terah \
    && zephir fullclean \
    && zephir build \
    && echo "extension=terah.so" > /usr/local/etc/php/conf.d/terah.ini

CMD ["php"]

