#!/usr/bin/env bash


if (( $# < 1 ))
then
        echo "Missing options!"
        echo "(run $0 -h for help)"
        echo ""
        exit 0
fi

DOCKER_REGISTRY_HOST=
DOCKER_REGISTRY_GROUP=
DOCKER_REGISTRY_USER=
DOCKER_REGISTRY_PASS=

. ./.env

COMMAND=${1}
IMAGE_NAME="${DOCKER_REGISTRY_GROUP}/asrt"
FULL_IMAGE_NAME="${DOCKER_REGISTRY_HOST}/${IMAGE_NAME}:latest"
IMAGE_URL="https://${DOCKER_REGISTRY_HOST}/${IMAGE_NAME}:latest"


function dump {

    ( set -o posix ; set ) | less

    exit 1
}


function login {

    docker login --username ${DOCKER_REGISTRY_USER} --password ${DOCKER_REGISTRY_PASS} ${DOCKER_REGISTRY_HOST}
}

function build {

    echo "docker build . -t ${IMAGE_NAME}"
    docker build . -t ${IMAGE_NAME}
    echo "docker tag ${IMAGE_NAME}:latest ${FULL_IMAGE_NAME}"
    docker tag ${IMAGE_NAME}:latest ${FULL_IMAGE_NAME}
}

function push {

    echo "docker push ${FULL_IMAGE_NAME}"
    docker push ${FULL_IMAGE_NAME}
}

function bash {

    echo "docker run --name=\"asrt\" --rm -it -v \"$(pwd)/src:/src\" -e \"PHP_IDE_CONFIG=asrt.local\" -e \"XDEBUG_CONFIG=idekey=PHPSTORM\" ${IMAGE_NAME} bash"
    docker run --name="asrt"  --rm -it -v "$(pwd)/src:/src" -e "PHP_IDE_CONFIG=serverName=asrt.local" -e "XDEBUG_CONFIG=idekey=PHPSTORM" ${IMAGE_NAME} bash
}

function exec {

    echo "docker exec -it asrt ${@}"
    docker exec -it asrt "${@}"
}

function zephir {

  echo "docker run -it ${IMAGE_NAME} /root/.composer/vendor/bin/zephir ${@}"
  docker run -it -v "$(pwd)/src:/src/" ${IMAGE_NAME} /root/.composer/vendor/bin/zephir "${@}"
}

function runTest {

    echo "docker run --name=\"asrt\" --rm -it -v \"$(pwd)/src:/src\" -e \"PHP_IDE_CONFIG=asrt.local\" -e \"XDEBUG_CONFIG=idekey=PHPSTORM\" ${IMAGE_NAME} /src/src/tester ${@}"
    docker run --name="asrt"  --rm -it -v "$(pwd)/src:/src" -e "PHP_IDE_CONFIG=serverName=asrt.local" -e "XDEBUG_CONFIG=idekey=PHPSTORM" ${IMAGE_NAME} /src/src/tester "${@}"
}


case ${COMMAND} in

        dump)

            dump
            ;;

        build)

            build
            ;;

        push)

            push
            ;;

        bash)

            bash
            ;;

        exec)

            exec "${@:2}"
            ;;

        login)

            login
            ;;

        buildAndPush)

            build
            push
            ;;

        zephir)

            zephir "${@:2}"
            ;;

        runTest)

            runTest "${@:2}"
            ;;

esac
