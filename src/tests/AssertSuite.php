<?php /** @noinspection PhpUnusedParameterInspection */
declare(strict_types=1);

namespace Terah\Asrt\Test;

/** @global Tester $tester */
use Terah\Asrt\Asrt;
use Terah\Asrt\AssertionFailedException;
use Terah\Tester\Tester;
use Terah\Tester\Suite;
use Terah\Asrt\ValidationFailedException;
use DateTime;
use stdClass;
use ArrayObject;
use Countable;

//$tester                 = $tester ??0?: new Tester;
$tester->suite('AsrtSuite')

    ->fixture('InvalidFloats', function() {

        return [1, false, 'test', null, '1.23', '10'];
    })

    ->fixture('InvalidIntegers', function() {

        return [1.23, false, 'test', null, '1.23', '10', new DateTime()];
    })

    ->fixture('InvalidIntegerish', function() {

        return [1.23, false, 'test', null, '1.23'];
    })

    ->fixture('InvalidEmpty', function() {

        return ['foo', true, 12, ['foo'], new stdClass()];
    })

    ->fixture('InvalidNotEmpty', function() {

        return ['', false, 0, null, [] ];
    })

    ->fixture('InvalidString', function() {

        return [1.23, false, new ArrayObject, null, 10, true];
    })

    ->fixture('ValidUrl', function() {

        return [
            'straight with Http'                                    => 'http://example.org',
            'Http with path'                                        => 'http://example.org/do/something',
            'Http with query'                                       => 'http://example.org/index.php?do=something',
            'Http with port'                                        => 'http://example.org:8080',
            'Http with all possibilities'                           => 'http://example.org:8080/do/something/index.php?do=something',
            'straight with Https'                                   => 'https://example.org',
        ];
    })

    ->fixture('InvalidUrl', function() {

        return [
            'null value'                                            => '',
            'empty string'                                          => ' ',
            'no scheme'                                             => 'url.de',
            'unsupported scheme'                                    => 'git://url.de',
            'Http with query (no / between tld und ?)'              => 'http://example.org?do=something',
            'Http with query and port (no / between port und ?)'    => 'http://example.org:8080?do=something',
        ];
    })

    ->fixture('InvalidChoicesForValueEmpty', function() {

        return [
            'empty values'                                          => [[], ['tux']],
            'empty recodes in $values'                              => [['tux' => ''], ['tux']]
        ];
    })
    ->fixture('ValidLengthUtf8Characters', function() {

        return [
            '址'                                                    => 1,
            'ل'                                                     => 1,
        ];
    })

    ->fixture('InvalidArray', function() {

        return [null, false, "test", 1, 1.23, new stdClass, fopen('php://memory', 'r'), 0];
    })

    ->fixture('ValidIsJsonString', function() {

        return [
            '»null« value'                                          => json_encode(null),
            '»false« value'                                         => json_encode(false),
            'array value'                                           => '["false"]',
            'object value'                                          => '{"tux":"false"}',
        ];
    })

    ->fixture('InvalidIsJsonString', function() {

        return [
            'no json string'                                        => 'invalid json encoded string',
            'error in json string'                                  => '{invalid json encoded string}',
        ];
    })

    ->fixture('ValidUuids', function() {

        return [
            'ff6f8cb0-c57d-11e1-9b21-0800200c9a66',
            'ff6f8cb0-c57d-21e1-9b21-0800200c9a66',
            'ff6f8cb0-c57d-31e1-9b21-0800200c9a66',
            'ff6f8cb0-c57d-41e1-9b21-0800200c9a66',
            'ff6f8cb0-c57d-51e1-9b21-0800200c9a66',
            'FF6F8CB0-C57D-11E1-9B21-0800200C9A66',
        ];
    })

    ->fixture('InvalidSamAccountName', function() {

        return [
            'johncitizen12345678999999999999',
            'johncitizen@something.com',
            'john.citizen',
            'citizen,john',
        ];
    })

    ->fixture('InvalidUncs', function() {

        return [
            '\\server\\someFolder',
            'server\\somePath',
            '\\\\\\server\\\\somePath',
        ];
    })

    ->fixture('InvalidDriveLetters', function() {

        return [
            'A drive',
            'A:\\',
            'A',
        ];
    })

    ->fixture('InvalidUuids', function() {

        return [
            'zf6f8cb0-c57d-11e1-9b21-0800200c9a66',
            'af6f8cb0c57d11e19b210800200c9a66',
            'ff6f8cb0-c57da-51e1-9b21-0800200c9a66',
            'af6f8cb-c57d-11e1-9b21-0800200c9a66',
            '3f6f8cb0-c57d-11e1-9b21-0800200c9a6',
        ];
    })

    ->fixture('InvalidCount', function() {

        return [
            [['Hi', 'There'], 3],
            [new class implements Countable {public function count(){return 1;}}, 2],
        ];
    })

    ->fixture('InvalidChoicesForInvalidKeySet', function() {

        return [
            'choice not found in values' => [
                ['tux' => ''],
                ['invalidChoice'],
                Asrt::INVALID_KEY_ISSET
            ]
        ];
    })

    ->test('testValidFloat', function(Suite $suite) {

        Asrt::that(1.0)->isFloat();
        Asrt::that(0.1)->isFloat();
        Asrt::that(-1.1)->isFloat();
    })

    ->test('testInvalidFloat', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidFloats') as $value )
        {
            Asrt::that($value)->isFloat();
        }

    }, '', Asrt::INVALID_FLOAT, AssertionFailedException::class)

    ->test('testValidInteger', function(Suite $suite) {

        Asrt::that(10)->isInt();
        Asrt::that(0)->isInt();
    })

    ->test('testInvalidInteger', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidIntegers') as $value )
        {
            Asrt::that($value)->isInt();
        }

    }, '', Asrt::INVALID_INTEGER, AssertionFailedException::class)

    ->test('testValidIntegerish', function(Suite $suite) {

        Asrt::that(10)->integerish();
        Asrt::that("10")->integerish();
    })

    ->test('testInvalidIntegerish', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidIntegerish') as $value )
        {
            Asrt::that($value)->integerish();
        }

    }, '', Asrt::INVALID_INTEGERISH, AssertionFailedException::class)

    ->test('testValidBoolean', function(Suite $suite) {

        Asrt::that(true)->isBool();
        Asrt::that(false)->isBool();
    })

    ->test('testInvalidBoolean', function(Suite $suite) {

        Asrt::that(1)->isBool();

    }, '', Asrt::INVALID_BOOLEAN, AssertionFailedException::class)

    ->test('testValidScalar', function(Suite $suite) {

        Asrt::that("foo")->scalar();
        Asrt::that(52)->scalar();
        Asrt::that(12.34)->scalar();
        Asrt::that(false)->scalar();
    })

    ->test('testInvalidScalar', function(Suite $suite) {

        Asrt::that(new \stdClass)->scalar();

    }, '', Asrt::INVALID_SCALAR, AssertionFailedException::class)

    ->test('testValidNotEmpty', function(Suite $suite) {

        Asrt::that("test")->notEmpty();
        Asrt::that(1)->notEmpty();
        Asrt::that(true)->notEmpty();
        Asrt::that(['foo'])->notEmpty();
    })

    ->test('testInvalidNotEmpty', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidNotEmpty') as $value )
        {
            Asrt::that($value)->notEmpty();
        }

    }, '', Asrt::VALUE_EMPTY, AssertionFailedException::class)

    ->test('testValidEmpty', function(Suite $suite) {

        Asrt::that("")->noContent();
        Asrt::that(0)->noContent();
        Asrt::that(false)->noContent();
        Asrt::that([])->noContent();
    })

    ->test('testInvalidEmpty', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidEmpty') as $value )
        {
            Asrt::that($value)->noContent();
        }

    }, '', Asrt::VALUE_NOT_EMPTY, AssertionFailedException::class)

    ->test('testValidNotNull', function(Suite $suite) {

        Asrt::that("1")->notNull();
        Asrt::that(1)->notNull();
        Asrt::that(0)->notNull();
        Asrt::that([])->notNull();
        Asrt::that(false)->notNull();
    })

    ->test('testInvalidNotNull', function(Suite $suite) {

        Asrt::that(null)->notNull();

    }, '', Asrt::VALUE_NULL, AssertionFailedException::class)

    ->test('testValidString', function(Suite $suite) {

        Asrt::that("test-string")->isString();
        Asrt::that("")->isString();
    })

    ->test('testInvalidString', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidString') as $value )
        {
            Asrt::that($value)->isString();
        }

    }, '', Asrt::INVALID_STRING, AssertionFailedException::class)

    ->test('testValidRegex', function(Suite $suite) {

        Asrt::that('uqcullen')->regex("/^[a-z0-9]{4,8}$/");

    })

    ->test('testInvalidRegex', function(Suite $suite) {

        Asrt::that("foo")->regex("bar)");

    }, '', Asrt::INVALID_NOT_VALID_REGEX, AssertionFailedException::class)

    ->test('testInvalidRegexValueNotString', function(Suite $suite) {

        Asrt::that(['foo'])->regex("(bar)");

    }, '', Asrt::INVALID_STRING, AssertionFailedException::class)

    ->test('testValidMinLength', function(Suite $suite) {

        Asrt::that("foo")->minLength(3);
        Asrt::that("foo")->minLength(1);
        Asrt::that("foo")->minLength(0);
        Asrt::that("")->minLength(0);
        Asrt::that("址址")->minLength(2);
    })

    ->test('testInvalidMinLength', function(Suite $suite) {

        Asrt::that("foo")->minLength(4);

    }, '', Asrt::INVALID_MIN_LENGTH, AssertionFailedException::class)

    ->test('testValidMaxLength', function(Suite $suite) {

        Asrt::that("foo")->maxLength(10);
        Asrt::that("foo")->maxLength(3);
        Asrt::that("")->maxLength(0);
        Asrt::that("址址")->maxLength(2);
    })

    ->test('testInvalidMaxLength', function(Suite $suite) {

        Asrt::that("foo")->maxLength(2);

    }, '', Asrt::INVALID_MAX_LENGTH, AssertionFailedException::class)

    ->test('testValidBetweenLength', function(Suite $suite) {

        Asrt::that("foo")->betweenLength(0, 3);
        Asrt::that("址址")->betweenLength(2, 2);
    })

    ->test('testInvalidBetweenLengthMin', function(Suite $suite) {

        Asrt::that("foo")->betweenLength(4, 100);

    }, '', Asrt::INVALID_MIN_LENGTH, AssertionFailedException::class)

    ->test('testInvalidBetweenLengthMax', function(Suite $suite) {

        Asrt::that("foo")->betweenLength(0, 2);

    }, '', Asrt::INVALID_MAX_LENGTH, AssertionFailedException::class)

    ->test('testValidStartsWith', function(Suite $suite) {

        Asrt::that("foo")->startsWith("foo");
        Asrt::that("foo")->startsWith("fo");
        Asrt::that("foo")->startsWith("f");
        Asrt::that("址foo")->startsWith("址");
    })

    ->test('testInvalidStartsWith', function(Suite $suite) {

        Asrt::that("foo")->startsWith("bar");

    }, '', Asrt::INVALID_STRING_START, AssertionFailedException::class)

    ->test('testInvalidStartsWithDueToWrongEncoding', function(Suite $suite) {

        Asrt::that("址")->startsWith("址址", '', '', 'ASCII');

    }, '', Asrt::INVALID_STRING_START, AssertionFailedException::class)

    ->test('testValidEndsWith', function(Suite $suite) {

        Asrt::that("foo")->endsWith("foo");
        Asrt::that("sonderbar")->endsWith("bar");
        Asrt::that("opp")->endsWith("p");
        Asrt::that("foo址")->endsWith("址");
    })

    ->test('testInvalidEndsWith', function(Suite $suite) {

        Asrt::that("foo")->endsWith("bar");

    }, '', Asrt::INVALID_STRING_END, AssertionFailedException::class)

    ->test('testInvalidEndsWithDueToWrongEncoding', function(Suite $suite) {

        Asrt::that("址")->endsWith("址址", '', '', 'ASCII');

    }, '', Asrt::INVALID_STRING_END, AssertionFailedException::class)

    ->test('testValidContains', function(Suite $suite) {

        Asrt::that("foo")->contains("foo");
        Asrt::that("foo")->contains("oo");
    })

    ->test('testInvalidContains', function(Suite $suite) {

        Asrt::that("foo")->contains("bar");
    
    }, '', Asrt::INVALID_STRING_CONTAINS, AssertionFailedException::class)
    
    ->test('testValidChoice', function(Suite $suite) {

        Asrt::that("foo")->choice(['foo']);
    })

    ->test('testInvalidChoice', function(Suite $suite) {

        Asrt::that("foo")->choice(["bar", "baz"]);
    
    }, '', Asrt::INVALID_CHOICE, AssertionFailedException::class)


    ->test('testValidInArray', function(Suite $suite) {

        Asrt::that("foo")->inArray(['foo']);
    })

    ->test('testInvalidInArray', function(Suite $suite) {

        Asrt::that("bar")->inArray(["baz"]);
    
    }, '', Asrt::INVALID_CHOICE, AssertionFailedException::class)

    ->test('testValidNumeric', function(Suite $suite) {

        Asrt::that("1")->numeric();
        Asrt::that(1)->numeric();
        Asrt::that(1.23)->numeric();
    })

    ->test('testInvalidNumeric', function(Suite $suite) {

        Asrt::that("foo")->numeric();
    
    }, '', Asrt::INVALID_NUMERIC, AssertionFailedException::class)

    ->test('testValidArray', function(Suite $suite) {

        Asrt::that([])->isArray();
        Asrt::that([1,2,3])->isArray();
        Asrt::that([[],[]])->isArray();
    })

    ->test('testInvalidArray', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidArray') as $value )
        {
            Asrt::that($value)->isArray();
        }
    
    }, '', Asrt::INVALID_ARRAY, AssertionFailedException::class)

    ->test('testValidKeyExists', function(Suite $suite) {

        Asrt::that(["foo" => "bar"])->keyExists("foo");
    })

    ->test('testInvalidKeyExists', function(Suite $suite) {

        Asrt::that(["foo" => "bar"])->keyExists("baz");
    
    }, '', Asrt::INVALID_KEY_EXISTS, AssertionFailedException::class)

    ->test('testValidNotBlank', function(Suite $suite) {

        Asrt::that("foo")->notBlank();
    })

    ->test('testInvalidNotBlank', function(Suite $suite) {

        Asrt::that("")->notBlank();
    
    }, '', Asrt::INVALID_NOT_BLANK, AssertionFailedException::class)

    ->test('testValidNotIsInstanceOf', function(Suite $suite) {

        Asrt::that(new \stdClass)->notIsInstanceOf('PDO');
    })

    ->test('testInvalidNotInstanceOf', function(Suite $suite) {

        Asrt::that(new \stdClass)->notIsInstanceOf('stdClass');
    
    }, '', Asrt::INVALID_NOT_INSTANCE_OF, AssertionFailedException::class)

    ->test('testValidInstanceOf', function(Suite $suite) {

        Asrt::that(new \stdClass)->isInstanceOf('stdClass');
    })

    ->test('testInvalidInstanceOf', function(Suite $suite) {

        Asrt::that(new \stdClass)->isInstanceOf('PDO');
    
    }, '', Asrt::INVALID_INSTANCE_OF, AssertionFailedException::class)

    ->test('testValidSubclassOf', function(Suite $suite) {

        Asrt::that(new class extends \stdClass{})->subclassOf('stdClass');
    })

    ->test('testInvalidSubclassOf', function(Suite $suite) {

        Asrt::that(new \stdClass)->subclassOf('PDO');
    
    }, '', Asrt::INVALID_SUBCLASS_OF, AssertionFailedException::class)

    ->test('testValidRange', function(Suite $suite) {

        Asrt::that(1)->range(1, 2);
        Asrt::that(2)->range(1, 2);
        Asrt::that(2)->range(0, 100);
        Asrt::that(2.5)->range(2.25, 2.75);
    })

    ->test('testInvalidRange', function(Suite $suite) {

        Asrt::that(1)->range(2, 3);
        Asrt::that(1.5)->range(2, 3);
    
    }, '', Asrt::INVALID_RANGE, AssertionFailedException::class)

    ->test('testValidEmail', function(Suite $suite) {

        Asrt::that("123hello+world@email.provider.com")->email();
    })

    ->test('testInvalidEmail', function(Suite $suite) {

        Asrt::that("foo")->email();
    
    }, '', Asrt::INVALID_EMAIL, AssertionFailedException::class)

    ->test('testValidUserPrincipalName', function(Suite $suite) {

        Asrt::that("johncitizen@email.provider.com")->userPrincipalName();
    })

    ->test('testInvalidUserPrincipalName', function(Suite $suite) {

        Asrt::that("johncitizen")->userPrincipalName();
    
    }, '', Asrt::INVALID_USERPRINCIPALNAME, AssertionFailedException::class)

    ->test('testValidUrl', function(Suite $suite) {

        foreach ( $suite->getFixture('ValidUrl') as $value )
        {
            Asrt::that($value)->url();
        }
    })

    ->test('testInvalidUrl', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidUrl') as $value )
        {
            Asrt::that($value)->url();
        }

    }, '', Asrt::INVALID_URL, AssertionFailedException::class)

    ->test('testValidDigit', function(Suite $suite) {

        Asrt::that(1)->digit();
        Asrt::that(0)->digit();
        Asrt::that("0")->digit();
    })

    ->test('testInvalidDigit', function(Suite $suite) {

        Asrt::that(-1)->digit();
    
    }, '', Asrt::INVALID_DIGIT, AssertionFailedException::class)

    ->test('testValidAlnum', function(Suite $suite) {

        Asrt::that("a")->alnum();
        Asrt::that("a1")->alnum();
        Asrt::that("aasdf1234")->alnum();
        Asrt::that("a1b2c3")->alnum();
    })

    ->test('testInvalidAlnum', function(Suite $suite) {

        Asrt::that("1a")->alnum();
    
    }, '', Asrt::INVALID_ALNUM, AssertionFailedException::class)

    ->test('testValidTrue', function(Suite $suite) {

        Asrt::that(1 == 1)->isTrue();
    })

    ->test('testInvalidTrue', function(Suite $suite) {

        Asrt::that(false)->isTrue();
    
    }, '', Asrt::INVALID_TRUE, AssertionFailedException::class)

    ->test('testValidFalse', function(Suite $suite) {

        Asrt::that(1 == 0)->isFalse();
    })

    ->test('testInvalidFalse', function(Suite $suite) {

        Asrt::that(true)->isFalse();
    
    }, '', Asrt::INVALID_FALSE, AssertionFailedException::class)

    ->test('testValidClass', function(Suite $suite) {

        Asrt::that("\\Exception")->classExists();
    })

    ->test('testInvalidClass', function(Suite $suite) {

        Asrt::that("Foo")->classExists();
    
    }, '', Asrt::INVALID_CLASS, AssertionFailedException::class)

    ->test('testValidSame', function(Suite $suite) {

        Asrt::that(1)->same(1);
        Asrt::that("foo")->same("foo");
        Asrt::that($obj = new \stdClass())->same($obj);
    })

    ->test('testInvalidSame', function(Suite $suite) {

        Asrt::that(new \stdClass())->same(new \stdClass());
    
    }, '', Asrt::INVALID_SAME, AssertionFailedException::class)

    ->test('testValidEq', function(Suite $suite) {

        Asrt::that(1)->eq("1");
        Asrt::that("foo")->eq(true);
        Asrt::that($obj = new \stdClass())->eq($obj);
    })

    ->test('testInvalidEq', function(Suite $suite) {

        Asrt::that("2")->eq(1);
    
    }, '', Asrt::INVALID_EQ, AssertionFailedException::class)

    ->test('testValidNotEq', function(Suite $suite) {

        Asrt::that("1")->notEq(false);
        Asrt::that(new \stdClass())->notEq([]);
    })

    ->test('testInvalidNotEq', function(Suite $suite) {

        Asrt::that("1")->notEq(1);
    
    }, '', Asrt::INVALID_NOT_EQ, AssertionFailedException::class)

    ->test('testValidNotSame', function(Suite $suite) {

        Asrt::that("1")->notSame(2);
        Asrt::that(new \stdClass())->notSame([]);
    })

    ->test('testInvalidNotSame', function(Suite $suite) {

        Asrt::that(1)->notSame(1);
    
    }, '', Asrt::INVALID_NOT_SAME, AssertionFailedException::class)

    ->test('testValidMin', function(Suite $suite) {

        Asrt::that(1)->min(1);
        Asrt::that(2)->min(1);
        Asrt::that(2.5)->min(1);
    })

    ->test('testInvalidMin', function(Suite $suite) {

        Asrt::that(0)->min(1);
    
    }, '', Asrt::INVALID_MIN, AssertionFailedException::class)

    ->test('testValidMax', function(Suite $suite) {

        Asrt::that(1)->max(1);
        Asrt::that(0.5)->max(1);
        Asrt::that(0)->max(1);
    })

    ->test('testInvalidMax', function(Suite $suite) {

        Asrt::that(2)->max(1);
    
    }, '', Asrt::INVALID_MAX, AssertionFailedException::class)

    ->test('testNullOr', function(Suite $suite) {

        Asrt::that(null)->nullOr()->max(1);
        Asrt::that(null)->nullOr()->max(2);
    })

    ->test('testValidLength', function(Suite $suite) {

        Asrt::that("asdf")->length(4);
        Asrt::that("")->length(0);
    })

    ->test('testInvalidLength', function(Suite $suite) {

        Asrt::that("asdf")->length(3);
    
    }, '', Asrt::INVALID_LENGTH, AssertionFailedException::class)

    ->test('testValidLengthUtf8Characters', function(Suite $suite) {

        foreach ( $suite->getFixture('ValidLengthUtf8Characters') as $value => $expected )
        {
            Asrt::that($value)->length($expected);
        }
    })

    ->test('testInvalidLengthForWrongEncoding', function(Suite $suite) {

        Asrt::that("址")->length(1, '', '', 'ASCII');
    
    }, '', Asrt::INVALID_LENGTH, AssertionFailedException::class)

    ->test('testValidLengthForGivenEncoding', function(Suite $suite) {

        Asrt::that("址")->length(1, '', '', 'utf8');
    })

    ->test('testValidFile', function(Suite $suite) {

        Asrt::that(__FILE__)->file();
    })

    ->test('testInvalidFileForEmptyFilename', function(Suite $suite) {

        Asrt::that("")->file();

    }, '', Asrt::VALUE_EMPTY, AssertionFailedException::class)

    ->test('testInvalidFileForDoesNotExist', function(Suite $suite) {

        Asrt::that(__DIR__ . '/does-not-exists')->file();

    }, '', Asrt::INVALID_FILE, AssertionFailedException::class)

    ->test('testValidDirectory', function(Suite $suite) {

        Asrt::that(__DIR__)->directory();
    })

    ->test('testInvalidDirectory', function(Suite $suite) {

        Asrt::that(__DIR__ . '/does-not-exist')->directory();

    }, '', Asrt::INVALID_DIRECTORY, AssertionFailedException::class)

    ->test('testValidReadable', function(Suite $suite) {

        Asrt::that(__FILE__)->readable();
    })

    ->test('testInvalidReadable', function(Suite $suite) {

        Asrt::that(__DIR__ . '/does-not-exist')->readable();

    }, '', Asrt::INVALID_READABLE, AssertionFailedException::class)

    ->test('testValidWriteable', function(Suite $suite) {

        Asrt::that(sys_get_temp_dir())->writeable();
    })

    ->test('testInvalidWriteable', function(Suite $suite) {

        Asrt::that(__DIR__ . '/does-not-exist')->writeable();

    }, '', Asrt::INVALID_WRITEABLE, AssertionFailedException::class)

    ->test('testValidImplementsInterface', function(Suite $suite) {

        Asrt::that('\ArrayIterator')->implementsInterface('\Traversable');
    })

    ->test('testInvalidImplementsInterface', function(Suite $suite) {

        Asrt::that('\Exception')->implementsInterface('\Traversable');

    }, '', Asrt::INTERFACE_NOT_IMPLEMENTED, AssertionFailedException::class)

    ->test('testValidImplementsInterfaceWithClassObject', function(Suite $suite) {

        $class = new \ArrayObject();

        Asrt::that($class)->implementsInterface('\Traversable');
    })

    ->test('testInvalidImplementsInterfaceWithClassObject', function(Suite $suite) {

        $class = new \ArrayObject();

        Asrt::that($class)->implementsInterface('\SplObserver');

    }, '', Asrt::INTERFACE_NOT_IMPLEMENTED, AssertionFailedException::class)

    ->test('testValidIsJsonString', function(Suite $suite) {

        foreach ( $suite->getFixture('ValidIsJsonString') as $value )
        {
            Asrt::that($value)->isJsonString();
        }
    })

    ->test('testInvalidIsJsonString', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidIsJsonString') as $value )
        {
            Asrt::that($value)->isJsonString();
        }

    }, '', Asrt::INVALID_JSON_STRING, AssertionFailedException::class)

    ->test('testValidSamAccountName', function(Suite $suite) {

        Asrt::that('johncitizen')->samAccountName();
        Asrt::that('jcitiz')->samAccountName();
        Asrt::that('jcitiz123')->samAccountName();
    })

    ->test('testInvalidSamAccountName', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidSamAccountName') as $value )
        {
            Asrt::that($value)->samAccountName();
        }

    }, '', Asrt::INVALID_SAMACCOUNTNAME, AssertionFailedException::class)

    ->test('testValidUnc', function(Suite $suite) {

        Asrt::that('\\\\some.server\\folderName')->unc();
        Asrt::that('\\\\some.server\\folderName\\someOtherFolderName')->unc();
    })

    ->test('testInvalidUnc', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidUncs') as $value )
        {
            Asrt::that($value)->unc();
        }

    }, '', Asrt::INVALID_UNC_PATH, AssertionFailedException::class)

    ->test('testValidDriveLetter', function(Suite $suite) {

        Asrt::that('A:')->driveLetter();
        Asrt::that('h:')->driveLetter();
    })

    ->test('testInvalidDriveLetter', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidDriveLetters') as $value )
        {
            Asrt::that($value)->driveLetter();
        }

    }, '', Asrt::INVALID_DRIVE_LETTER, AssertionFailedException::class)


    ->test('testValidUuids', function(Suite $suite) {

        foreach ( $suite->getFixture('ValidUuids') as $value )
        {
            Asrt::that($value)->uuid();
        }
    })

    ->test('testInvalidUuids', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidUuids') as $value )
        {
            Asrt::that($value)->uuid();
        }

    }, '', Asrt::INVALID_UUID, AssertionFailedException::class)

    ->test('testValidNotEmptyKey', function(Suite $suite) {

        Asrt::that(['keyExists' => 'notEmpty'])->notEmptyKey('keyExists');
    })

    ->test('testInvalidNotEmptyKey', function(Suite $suite) {

        $tests                  = [
            'testKey'               => ['testKey'   => 'I am not empty'],
            'testKey2'              => ['testKey2'  => ['I am not empty', 'I am not empty']],
        ];
        foreach ( $tests as $key => $value )
        {
            Asrt::that($value)->notEmptyKey($key);
        }
    })

    ->test('testAllWithSimpleAssertion', function(Suite $suite) {

        Asrt::that([true, true])->all()->isTrue();
    })

    ->test('testAllWithSimpleAssertionThrowsExceptionOnElementThatFailsAssertion', function(Suite $suite) {

        Asrt::that([true, false])->all()->isTrue();

    }, '', Asrt::INVALID_TRUE, AssertionFailedException::class)

    ->test('testAllWithComplexAssertion', function(Suite $suite) {

        Asrt::that([new \stdClass, new \stdClass])->all()->isInstanceOf('stdClass');
    })

    ->test('testAllWithComplexAssertionThrowsExceptionOnElementThatFailsAssertion', function(Suite $suite) {

        Asrt::that([new \stdClass, new \stdClass])->all()->isInstanceOf('PDO', 'Assertion failed', 'foos');

    }, '', Asrt::INVALID_INSTANCE_OF, AssertionFailedException::class)

    ->test('testAllWithNoValueThrows', function(Suite $suite) {

        Asrt::that(null)->all()->isTrue();

    }, '', Asrt::INVALID_TRAVERSABLE, AssertionFailedException::class)

    ->test('testValidCount', function(Suite $suite) {

        Asrt::that(['Hi'])->count(1);
        Asrt::that(new class implements \Countable {public function count(){return 1;}})->count(1);
    })

    ->test('testInvalidCount', function(Suite $suite) {

        foreach ( $suite->getFixture('InvalidCount') as $key => $value )
        {
            Asrt::that($value)->count($key);
        }

    }, '', Asrt::INVALID_COUNT, AssertionFailedException::class)

    ->test('testChoicesNotEmpty', function(Suite $suite) {

        Asrt::that(['tux' => 'linux', 'Gnu' => 'dolphin'])->choicesNotEmpty(['tux']);
    })

    ->test('testInvalidChoicesNotEmptyForValueEmpty', function(Suite $suite) {

        $tests = [
            'choice not found in values' => [['tux' => ''], ['invalidChoice'], Asrt::INVALID_KEY_ISSET]
        ];
        foreach ( $tests as $key => $value )
        {
            Asrt::that($key)->choicesNotEmpty($value);
        }

    }, '', Asrt::INVALID_ARRAY_ACCESSIBLE, AssertionFailedException::class)

    ->test('testInvalidChoicesNotEmptyForInvalidKeySet', function(Suite $suite) {

        $test =  [
            'empty values' => [[], ['tux']],
            'empty recodes in $values' => [['tux' => ''], ['tux']]
        ];
        foreach ( $test as $key => $value )
        {
            Asrt::that($key)->choicesNotEmpty($value);
        }

    }, '', Asrt::INVALID_ARRAY_ACCESSIBLE, AssertionFailedException::class)

    ->test('testValidIsObject', function(Suite $suite) {

        Asrt::that(new \stdClass)->isObject();
    })

    ->test('testInvalidIsObject', function(Suite $suite) {

        Asrt::that('notAnObject')->isObject();

    }, '', Asrt::INVALID_OBJECT, AssertionFailedException::class)

    ->test('testValidIsAusMobile', function(Suite $suite) {

        Asrt::that('0438333333')->ausMobile();
    })

    ->test('testInvalidIsAusMobile', function(Suite $suite) {

        Asrt::that('12341234')->ausMobile();

    }, '', Asrt::INVALID_AUS_MOBILE, AssertionFailedException::class)

    ->test('testValidMethodExists', function(Suite $suite) {

        Asrt::that('methodExists')->methodExists(Asrt::that(null));
    })

    ->test('testValidChaining', function(Suite $suite) {

        Asrt::that(1)->isInt()->integerish()->numeric()->notNull()->eq(1);
        Asrt::that([1,1,1,1,1,1,])->allIds()->integerish()->numeric()->notNull()->eq(1);
    })

    ->test('testChainingFails', function(Suite $suite) {

        Asrt::that(1)->isInt()->integerish()->numeric()->notNull()->eq(2);

    }, '', Asrt::INVALID_EQ, AssertionFailedException::class)

    ->test('testAllChainingFails', function(Suite $suite) {

        Asrt::that([1,1,1,1,1,2,])->all()->id()->integerish()->numeric()->notNull()->eq(1);

    }, '', Asrt::INVALID_EQ, AssertionFailedException::class)


    ->test('testDifferentExceptionError', function(Suite $suite) {

        Asrt::that(1)->setExceptionClass('ValidationFailedException')->eq(2);

    }, '', Asrt::INVALID_EQ, ValidationFailedException::class)

    ;