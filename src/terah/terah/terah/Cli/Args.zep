namespace Terah\Cli;

class Args
{
    protected args              = [];

    public function get(var name, var def=null)
    {
        if empty this->args
        {
            let this->args          = self::parseArgs();
        }

        if isset this->args[name] && this->args[name]
        {
            return this->args[name];
        }

        return def;
    }

    public static function parseArgs(array argv=[]) -> array
    {
        var args                = argv;
        if empty argv
        {
            let args                = [];
            if ! empty _SERVER["argv"]
            {
                let args                = _SERVER["argv"];
            }
        }
        var out                 = [];
        var i                   = 0;
        var cnt                 = count(argv);
        var arg;
        var key;
        for arg in args
        {
            // --foo --bar=baz
            if ( mb_substr(arg, 0, 2) === "--" )
            {
                var eqPos               = mb_strpos(arg, "=");
                // --foo
                if eqPos === false
                {
                    let key                 = mb_substr(arg, 2);
                    // --foo value
                    if i + 1 < cnt && args[i + 1][0] !== "-"
                    {
                        let out[key]            = args[i + 1];
                        let i                   = i + 1;
                    }
                    else
                    {
                        if ! isset(out[key])
                        {
                            let out[key]            = true;
                        }
                    }
                }
                // --bar=baz
                else
                {
                    let key                 = mb_substr(arg, 2, eqPos - 2);
                    let out[key]            = mb_substr(arg, eqPos + 1);
                }
            }
            // -k=value -abc
            elseif mb_substr(arg, 0, 1) === "-"
            {
                // -k=value
                if mb_substr(arg, 2, 1) === "="
                {
                    let key                 = mb_substr(arg, 1, 1);
                    let out[key]            = mb_substr(arg, 3);
                }
                // -abc
                else
                {
                    var chars               = str_split(mb_substr(arg, 1));
                    var chr;
                    for chr in chars
                    {
                        let key                 = chr;
                        if ! isset(out[key])
                        {
                            let out[key]            = true;
                        }
                    }
                    // -a value1 -abc value2
                    if i + 1 < cnt && args[i + 1][0] !== "-"
                    {
                        let out[key]            = args[i + 1];
                        let i                   = i + 1;
                    }
                }
            }
            // plain-arg
            else
            {
                let out[]               = arg;
            }
        }
        var idx;
        var val;
        for idx, val in out
        {
            if is_string(val) && strpos(val, "|") !== false
            {
                let out[idx]            = explode("|", val);
            }
        }

        return out;
    }
}