namespace Terah\Utils;

class Str
{

    public static function between(string start, string end, string hayStack) -> string
    {
        return self::before(end, self::after(start, hayStack));
    }


    public static function before(string needle, string hayStack, bool returnOrigIfNeedleNotExists=false) -> string
    {
        var position            = mb_strpos(hayStack, needle);
        if ( position === false )
        {
            return self::emptyOrHaystack(hayStack, returnOrigIfNeedleNotExists);
        }
        var result              = mb_substr(hayStack, 0, position);
        if ( ! result && returnOrigIfNeedleNotExists )
        {
            return hayStack;
        }

        return result;
    }


    public static function after(string needle, string hayStack, bool returnOrigIfNeedleNotExists=false) -> string
    {
        if ( ! is_bool(mb_strpos(hayStack, needle)) )
        {
            return mb_substr(hayStack, mb_strpos(hayStack, needle) + mb_strlen(needle));
        }

        return self::emptyOrHaystack(hayStack, returnOrigIfNeedleNotExists);
    }


    public static function betweenLast(string start, string end, string hayStack) -> string
    {
        return self::afterLast(start, self::beforeLast(end, hayStack));
    }


    public static function afterLast(string needle, string hayStack, bool returnOrigIfNeedleNotExists=false) -> string
    {
        if self::strrevpos(hayStack, needle) < 0
        {
            return self::emptyOrHaystack(hayStack, returnOrigIfNeedleNotExists);
        }

        return mb_substr(hayStack, self::strrevpos(hayStack, needle) + mb_strlen(needle));
    }


    public static function strrevpos(string str, string needle) -> int
    {
        var revStr              = mb_strpos(strrev(str), strrev(needle));
        if revStr === false
        {
            return -1;
        }

        return mb_strlen(str) - revStr - mb_strlen(needle);
    }


    public static function beforeLast(string needle, string hayStack) -> string
    {
        var position            = static::strrevpos(hayStack, needle);
        if position < 0
        {
            return "";
        }

        return mb_substr(hayStack, 0, position);
    }


    protected static function emptyOrHaystack(string hayStack, bool returnOrigIfNeedleNotExists) -> string
    {
        if returnOrigIfNeedleNotExists
        {
            return hayStack;
        }

        return "";
    }

}