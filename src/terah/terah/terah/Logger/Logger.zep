namespace Terah\Logger;

use Closure;
use Throwable;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\InvalidArgumentException;

class Logger implements LoggerInterface
{
    const EMERGENCY     = "emergency";
    const ALERT         = "alert";
    const CRITICAL      = "critical";
    const ERROR         = "error";
    const WARNING       = "warning";
    const NOTICE        = "notice";
    const INFO          = "info";
    const DEBUG         = "debug";

    const BLACK         = "black";
    const DARK_GRAY     = "dark_gray";
    const BLUE          = "blue";
    const LIGHT_BLUE    = "light_blue";
    const GREEN         = "green";
    const LIGHT_GREEN   = "light_green";
    const CYAN          = "cyan";
    const LIGHT_CYAN    = "light_cyan";
    const RED           = "red";
    const LIGHT_RED     = "light_red";
    const PURPLE        = "purple";
    const LIGHT_PURPLE  = "light_purple";
    const BROWN         = "brown";
    const YELLOW        = "yellow";
    const MAGENTA       = "magenta";
    const LIGHT_GRAY    = "light_gray";
    const WHITE         = "white";
    const DEF           = "default";
    const BOLD          = "bold";

    const IDX_LEVEL     = 0;
    const IDX_FORE      = 1;
    const IDX_BACK      = 2;
    const IDX_FONT      = 3;
    const IDX_LABEL     = 4;

    /**  @var resource handle The file handle */
    protected handle         = null;

    /** @var string level */
    protected level            = self::INFO;

    /** @var bool closeLocally */
    protected closeLocally     = false;

    /** @var bool */
    protected addDate          = true;

    /** @var string  */
    protected separator        = " | ";

    /** @var \Closure */
    protected formatter        = null;

    /** @var string  */
    protected lastLogEntry     = "";

    /** @var bool|null  */
    protected gzipFile         = null;

    /** @var bool  */
    protected useLocking       = false;

    static protected logLevels = [
        "emergency"             : [1, self::WHITE,       self::RED,     self::DEF,      "EMERG"],
        "alert"                 : [2, self::WHITE,       self::YELLOW,  self::DEF,      "ALERT"],
        "critical"              : [3, self::RED,         self::DEF,     self::BOLD ,    "CRIT"],
        "error"                 : [4, self::RED,         self::DEF,     self::DEF,      "ERROR"],
        "warning"               : [5, self::YELLOW,      self::DEF,     self::DEF,      "WARN"],
        "notice"                : [6, self::CYAN,        self::DEF,     self::DEF,      "NOTE"],
        "info"                  : [7, self::GREEN,       self::DEF,     self::DEF,      "INFO"],
        "debug"                 : [8, self::LIGHT_GRAY,  self::DEF,     self::DEF,      "DEBUG"]
    ];

    static protected colours   = [
        "fore"                  : [
            "black"                 : "0;30",
            "dark_gray"             : "1;30",
            "blue"                  : "0;34",
            "light_blue"            : "1;34",
            "green"                 : "0;32",
            "light_green"           : "1;32",
            "cyan"                  : "0;36",
            "light_cyan"            : "1;36",
            "red"                   : "0;31",
            "light_red"             : "1;31",
            "purple"                : "0;35",
            "light_purple"          : "1;35",
            "brown"                 : "0;33",
            "yellow"                : "1;33",
            "magenta"               : "0;35",
            "light_gray"            : "0;37",
            "white"                 : "1;37"
        ],
        "back"                  : [
            "default"               : "49",
            "black"                 : "40",
            "red"                   : "41",
            "green"                 : "42",
            "yellow"                : "43",
            "blue"                  : "44",
            "magenta"               : "45",
            "cyan"                  : "46",
            "light_gray"            : "47"
        ],
        "bold"                  : []
    ];


    public function __construct(var! handle=null, string level=self::INFO, bool useLocking=false, bool gzipFile=false, bool addDate=true)
    {
        let this->handle        = handle;
        this->setLogLevel(level);
        let this->useLocking    = useLocking;
        let this->gzipFile      = gzipFile;
        let this->addDate       = addDate;
    }

    /**
     * System is unusable.
     *
     * @param string message
     * @param array context
     */
    public function emergency(message, array context = []) -> void
    {
        this->log(self::EMERGENCY, message, context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string message
     * @param array context
     */
    public function alert(message, array context = []) -> void
    {
        this->log(self::ALERT, message, context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string message
     * @param array context
     */
    public function critical(message, array context = []) -> void
    {
        this->log(self::CRITICAL, message, context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string message
     * @param array context
     */
    public function error(message, array context = []) -> void
    {
        this->log(self::ERROR, message, context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string message
     * @param array context
     */
    public function warning(message, array context = []) -> void
    {
        this->log(self::WARNING, message, context);
    }

    /**
     * Normal but significant events.
     *
     * @param string message
     * @param array context
     */
    public function notice(message, array context = []) -> void
    {
        this->log(self::NOTICE, message, context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string message
     * @param array context
     */
    public function info(message, array context = []) -> void
    {
        this->log(self::INFO, message, context);
    }

    /**
     * Detailed debug information.
     *
     * @param string message
     * @param array context
     */
    public function debug(message, array context = []) -> void
    {
        this->log(self::DEBUG, message, context);
    }


    public function setLogFile(string handle) -> <Logger>
    {
        let this->handle        = handle;

        return this;
    }


    public static function addColour(string strText, string foregroundColor="", string backgroundColor="", bool bold=false) -> string
    {
        // todo: support bold
        var coloredString       = "";
        // Check if given foreground color found
        if likely isset self::colours["fore"][foregroundColor]
        {
            let coloredString       = coloredString . "\e[" . self::colours["fore"][foregroundColor] . "m";
        }
        // Check if given background color found
        if isset(self::colours["back"][backgroundColor])
        {
            let coloredString       = coloredString . "\e[" . self::colours["back"][backgroundColor] . "m";
        }
        // Add string and end coloring
        // \033[0;31mtesting\033[0m
        let coloredString       = coloredString . strText . "\e[0m";

        return coloredString;
    }

    public static function toRed(string strText) -> string
    {
        return "\e[0;31m" . strText . "\e[0m";
    }


    public function colourize(string str, string foregroundColor="", string backgroundColor="", bool bold=false) -> string
    {
        return self::addColour(str, foregroundColor, backgroundColor, bold);
    }

    /**
     * @param string level Ignore logging attempts at a level less the level
     * @return Logger
     */
    public function setLogLevel(string level) -> <Logger>
    {
        if unlikely ! isset(self::logLevels[level])
        {
            throw new InvalidArgumentException("Log level is invalid");
        }
        let this->level         = self::logLevels[level][self::IDX_LEVEL];

        return this;
    }


    public function lock() -> <Logger>
    {
        let this->useLocking    = true;

        return this;
    }


    public function gzipped() -> <Logger>
    {
        let this->gzipFile      = true;

        return this;
    }


    public function formatter(callable fnFormatter) -> <Logger>
    {
        let this->formatter     = fnFormatter;

        return this;
    }

    /**
     * Log messages to handle
     *
     * @param mixed          level    The level of the log message
     * @param string         message  If an object is passed it must implement __toString()
     * @param array          context  Placeholders to be substituted in the message
     */
    public function log(level, message, array context = []) -> void
    {
        let level               = (string)level;
        let message             = (string)message;
        var levelMap            = self::getLevelMap(level);
        var logLevel            = levelMap[self::IDX_LEVEL];
        var fore                = levelMap[self::IDX_FORE];
        var back                = levelMap[self::IDX_BACK];
        var label               = levelMap[self::IDX_LABEL];
        if logLevel > this->level
        {
            return ;
        }
        if is_callable(this->formatter)
        {
            let message             = this->formatter->__invoke(label, message, context);
        }
        else
        {
            let message             = this->formatMessage(level, message, context);
        }
        let this->lastLogEntry  = message;
        this->write(this->colourize(message, fore, back) . PHP_EOL);
    }


    public static function style(string level, string message) -> string
    {
        var levelMap            = self::getLevelMap(level);
        var fore                = levelMap[self::IDX_FORE];
        var back                = levelMap[self::IDX_BACK];

        return self::addColour(message, fore, back);
    }


    protected function formatMessage(string level, string message, array context=[]) -> string
    {
        var levelMap            = self::getLevelMap(level);
        var cnxt                = "";
        if ! empty context
        {
            let cnxt                = PHP_EOL . json_encode(context, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
        var data                = [];
        if this->addDate
        {
            let data               = ["date" : date("Y-m-d H:i:s")];
        }
        let data["level"]       = strtoupper(str_pad(levelMap[self::IDX_LABEL], 5, " ", STR_PAD_RIGHT));
        let data["message"]     = message . cnxt;

        return implode(this->separator, data);
    }


    public function write(string content)
    {
        this->loadResource();
        if ( this->useLocking )
        {
            flock(this->handle, LOCK_EX);
        }
        this->writeToFile(content);
        if ( this->useLocking )
        {
            flock(this->handle, LOCK_UN);
        }
    }


    protected function writeToFile(string content)
    {
        gzwrite(this->handle, content);
    }


    protected function loadResource()
    {
        if is_resource(this->handle)
        {
            return;
        }
        var fileName            = this->handle;
        if is_null(fileName)
        {
            let fileName            = STDOUT;
        }
        let this->closeLocally  = true;
        if this->gzipFile
        {
            let this->handle        = gzopen(this->handle, "a");
        }
        else
        {
            let this->handle        = fopen(this->handle, "a");
        }
        if ( ! is_resource(this->handle) )
        {
            throw new Exception(sprintf("The handle (%s) could not be opened", fileName));
        }
    }


    public function getLastLogEntry() -> string
    {
        return this->lastLogEntry;
    }


    public function __destruct()
    {
        if this->closeLocally
        {
            this->loadResource();
            gzclose(this->handle);
        }
    }

    protected static function getLevelMap(string level) -> array
    {
        if ! isset self::logLevels[level]
        {
            let level               = self::INFO;
        }

        return self::logLevels[level];
    }
}