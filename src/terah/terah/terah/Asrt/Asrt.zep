namespace Terah\Asrt;

use DateTime;
use ReflectionClass;
use Traversable;
use ArrayAccess;
use ArrayObject;

class Asrt
{
    const ASRT_VERSION                  = "1.0.3-20191227";
    const ASSERTION_EXCEPTION           = "AssertionFailedException";
    const VALIDATION_EXCEPTION          = "ValidationFailedException";

    const INVALID_FLOAT                 = 1;
    const INVALID_INTEGER               = 2;
    const INVALID_DIGIT                 = 3;
    const INVALID_INTEGERISH            = 4;
    const INVALID_BOOLEAN               = 5;
    const VALUE_EMPTY                   = 6;
    const VALUE_NULL                    = 7;
    const INVALID_STRING                = 8;
    const INVALID_REGEX                 = 9;
    const INVALID_MIN_LENGTH            = 10;
    const INVALID_MAX_LENGTH            = 11;
    const INVALID_STRING_START          = 12;
    const INVALID_STRING_CONTAINS       = 13;
    const INVALID_CHOICE                = 14;
    const INVALID_NUMERIC               = 15;
    const INVALID_ARRAY                 = 16;
    const INVALID_KEY_EXISTS            = 17;
    const INVALID_NOT_BLANK             = 18;
    const INVALID_INSTANCE_OF           = 19;
    const INVALID_SUBCLASS_OF           = 20;
    const INVALID_RANGE                 = 21;
    const INVALID_ALNUM                 = 22;
    const INVALID_TRUE                  = 23;
    const INVALID_EQ                    = 24;
    const INVALID_SAME                  = 25;
    const INVALID_MIN                   = 26;
    const INVALID_MAX                   = 27;
    const INVALID_LENGTH                = 28;
    const INVALID_FALSE                 = 29;
    const INVALID_STRING_END            = 30;
    const INVALID_UUID                  = 31;
    const INVALID_COUNT                 = 32;
    const INVALID_NOT_EQ                = 33;
    const INVALID_NOT_SAME              = 34;
    const INVALID_TRAVERSABLE           = 35;
    const INVALID_ARRAY_ACCESSIBLE      = 36;
    const INVALID_KEY_ISSET             = 37;
    const INVALID_SAMACCOUNTNAME        = 38;
    const INVALID_USERPRINCIPALNAME     = 39;
    const INVALID_DIRECTORY             = 40;
    const INVALID_FILE                  = 41;
    const INVALID_READABLE              = 42;
    const INVALID_WRITEABLE             = 43;
    const INVALID_CLASS                 = 44;
    const INVALID_EMAIL                 = 45;
    const INTERFACE_NOT_IMPLEMENTED     = 46;
    const INVALID_URL                   = 47;
    const INVALID_NOT_INSTANCE_OF       = 48;
    const VALUE_NOT_EMPTY               = 49;
    const INVALID_JSON_STRING           = 50;
    const INVALID_OBJECT                = 51;
    const INVALID_METHOD                = 52;
    const INVALID_SCALAR                = 53;
    const INVALID_DATE                  = 54;
    const INVALID_CALLABLE              = 55;
    const INVALID_KEYS_EXIST            = 56;
    const INVALID_PROPERTY_EXISTS       = 57;
    const INVALID_PROPERTIES_EXIST      = 58;
    const INVALID_UTF8                  = 59;
    const INVALID_DOMAIN_NAME           = 60;
    const INVALID_NOT_FALSE             = 61;
    const INVALID_FILE_OR_DIR           = 62;
    const INVALID_ASCII                 = 63;
    const INVALID_NOT_REGEX             = 64;
    const INVALID_GREATER_THAN          = 65;
    const INVALID_LESS_THAN             = 66;
    const INVALID_GREATER_THAN_OR_EQ    = 67;
    const INVALID_LESS_THAN_OR_EQ       = 68;
    const INVALID_IP_ADDRESS            = 69;
    const INVALID_AUS_MOBILE            = 70;
    const INVALID_ISNI                  = 71;
    const INVALID_DATE_RANGE            = 72;
    const INVALID_UNC_PATH              = 73;
    const INVALID_DRIVE_LETTER          = 74;
    const INVALID_NOT_VALID_REGEX       = 75;

    const EMERGENCY                     = "emergency";
    const ALERT                         = "alert";
    const CRITICAL                      = "critical";
    const ERROR                         = "error";
    const WARNING                       = "warning";
    const NOTICE                        = "notice";
    const INFO                          = "info";
    const DEBUG                         = "debug";

    /** @var bool */
    protected nullOr                   = false;

    /** @var bool */
    protected emptyOr                  = false;

    /** @var mixed */
    protected value                    = null;

    /** @var bool */
    protected all                      = false;

    /** @var string */
    protected fieldName                = "";

    /** @var string */
    protected propertyPath             = "";

    /** @var string */
    protected level                    = "critical";

    /** @var int */
    protected overrideCode             = null;

    /** @var string */
    protected overrideError            = "";

    /**
     * Exception to throw when an assertion failed.
     *
     * @var string
     */
    protected exceptionClass           = "";


    public function __construct(var value, string fieldName="", int code=0, string error="", string level="warning")
    {
        this->value(value);
        this->setExceptionClass(Asrt::ASSERTION_EXCEPTION);
        if fieldName
        {
            this->fieldName(fieldName);
        }
        if code
        {
            this->code(code);
        }
        if error
        {
            this->error(error);
        }
        if level
        {
            this->level(level);
        }
    }


    public static function that(var value, string fieldName="", int code=0, string error="", string level="warning") -> <Asrt>
    {
        var asrt;
        let asrt                = new Asrt(value, fieldName, code, error, level);

        return asrt;
    }

    /**
     * @param \Closure[] validators
     * @return array
     */
    public static function runValidators(array validators) -> array
    {
        var errors              = [];
        var fieldName;
        var validator;
        var e;
        for fieldName, validator in validators
        {
            try
            {
                validator->__invoke();
            }
            catch AssertionFailedException, e
            {
                let errors[fieldName]     = e->getMessage();
            }
        }

        return errors;
    }


    public function reset(value) -> <Asrt>
    {
        return this->all(false)->nullOr(false)->value(value);
    }


    public function value(var value) -> <Asrt>
    {
        let this->value         = value;

        return this;
    }


    public function nullOr(bool nullOr=true) -> <Asrt>
    {
        let this->nullOr        = nullOr;

        return this;
    }


    public function emptyOr(bool emptyOr=true) -> <Asrt>
    {
        let this->emptyOr       = emptyOr;

        return this;
    }


    public function all(bool all=true) -> <Asrt>
    {
        let this->all           = all;

        return this;
    }


    public function getException(int code, string message, string defaultMessage, string fieldName="", array constraints=[], string level="") -> <AssertionFailedException>
    {
        var msg                 = message;
        // prefer the passed in message then the override message then the default message
        if likely empty message {

            let msg                 = this->getError(defaultMessage);
        }
        let msg                 = sprintf(msg, this->stringify(this->value));
        // prefer the overrideCode then the passed in code
        let code                = (int)this->getCode(code);
        // prefer the passed in field over the globally set one
        if likely empty fieldName
        {
            let fieldName           = this->getFieldName(fieldName);
        }
        if likely empty level
        {
            let level               = this->getLevel(level);
        }
        if this->getExceptionClass() === Asrt::VALIDATION_EXCEPTION
        {
            return new ValidationFailedException(msg, code, fieldName, this->value, constraints, level, this->propertyPath);
        }

        return new AssertionFailedException(msg, code, fieldName, this->value, constraints, level, this->propertyPath);
    }


    public function setExceptionClass(string exceptionClass) -> <Asrt>
    {
        let this->exceptionClass = exceptionClass;

        return this;
    }


    public function getExceptionClass(string def="") -> string
    {
        if unlikely ! empty this->exceptionClass
        {
            return (string)this->exceptionClass;
        }

        return def;
    }


    public function code(int code) -> <Asrt>
    {
        let this->overrideCode = code;

        return this;
    }


    public function getCode(int def) -> int
    {
        if unlikely ! empty this->overrideCode
        {
            return (int)this->overrideCode;
        }

        return def;
    }


    public function fieldName(string fieldName) -> <Asrt>
    {
        let this->fieldName = fieldName;

        return this;
    }


    public function getFieldName(string def) -> string
    {
        if unlikely ! empty this->fieldName
        {
            return (string)this->fieldName;
        }

        return def;
    }


    public function name(string fieldName) -> <Asrt>
    {
        let this->fieldName = fieldName;

        return this;
    }


    public function level(string level) -> <Asrt>
    {
        let this->level = level;

        return this;
    }


    public function getLevel(string def) -> string
    {
        if unlikely ! empty this->level
        {
            return (string)this->level;
        }

        return def;
    }


    public function error(string error) -> <Asrt>
    {
        let this->overrideError = error;

        return this;
    }


    public function getError(string def) -> string
    {
        if unlikely ! empty this->overrideError
        {
            return (string)this->overrideError;
        }

        return def;
    }


    public function propertyPath(string propertyPath) -> <Asrt>
    {
        let this->propertyPath = propertyPath;

        return this;
    }


    public function getPropertyPath(string def) -> string
    {
        if unlikely ! empty this->propertyPath
        {
            return (string)this->propertyPath;
        }

        return def;
    }


    public function eq(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value != value2
        {
            throw this->getException(Asrt::INVALID_EQ, message, "Value '%s' does not equal expected value '" . $this->stringify($value2) . "'.", fieldName);
        }

        return this;
    }


    public function greaterThan(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value <= value2
        {
            throw this->getException(Asrt::INVALID_EQ, message, "Value '%s' does not greater then expected value '%s'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function greaterThanOrEq(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value < value2
        {
            throw this->getException(Asrt::INVALID_EQ, message, "Value '%s' does not greater than or equal to expected value '%s'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function lessThan(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! ( this->value < value2 )
        {
            throw this->getException(Asrt::INVALID_LESS_THAN, message, "Value '%s' does not less then expected value '%s'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function lessThanOrEq(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! ( this->value <= value2 )
        {
            throw this->getException(Asrt::INVALID_LESS_THAN_OR_EQ, message, "Value '%s' does not less than or equal to expected value '%s'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function same(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value !== value2
        {
            throw this->getException(Asrt::INVALID_SAME, message, "Value '%s' is not the same as expected value '" . this->stringify(value2) . "'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function notEq(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value == value2
        {
            throw this->getException(Asrt::INVALID_NOT_EQ, message, "Value '%s' is equal to expected value '" . this->stringify(value2) . "'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function isCallable(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_callable(this->value)
        {
            throw this->getException(Asrt::INVALID_NOT_EQ, message, "Value '%s' is not callable.", fieldName);
        }

        return this;
    }


    public function notSame(value2, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value === value2
        {
            throw this->getException(Asrt::INVALID_NOT_SAME, message, "Value '%s' is the same as expected value '" . this->stringify(value2) . "'.", fieldName, ["expected": value2]);
        }

        return this;
    }


    public function id(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isInt(message, fieldName);
        if unlikely this->value < 1
        {
            throw this->getException(Asrt::INVALID_RANGE, message, "Value '%s' is not an integer id.", fieldName);
        }

        return this;
    }


    public function unsignedInt(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isInt(message, fieldName);
        if unlikely this->value < -1
        {
            throw this->getException(Asrt::INVALID_RANGE, message, "Value '%s' is not an unsigned integer.", fieldName);
        }

        return this;
    }


    public function flag(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->numeric(message, fieldName);
        if unlikely ! in_array(this->value,  [0, 1])
        {
            throw this->getException(Asrt::INVALID_RANGE, message, "Value '%s' is not a 0 or 1.", fieldName);
        }

        return this;
    }


    public function status(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->numeric(message, fieldName);
        if unlikely ! in_array(this->value,  [-1, 0, 1])
        {
            throw this->getException(Asrt::INVALID_RANGE, message, "Value '%s' is not a valid status.");
        }

        return this;
    }


    public function nullOrId(string message="", string fieldName="") -> <Asrt>
    {
        return this->nullOr()->id(message, fieldName);
    }



    public function allIds(string message="", string fieldName="") -> <Asrt>
    {
        return this->all()->id(message, fieldName);
    }


    public function isInt(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_int(this->value)
        {
            throw this->getException(Asrt::INVALID_INTEGER, message, "Value '%s' is not an integer.", fieldName);
        }

        return this;
    }


    public function isFloat(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_float(this->value)
        {
            throw this->getException(Asrt::INVALID_FLOAT, message, "Value '%s' is not a float.", fieldName);
        }

        return this;
    }


    public function digit(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! ctype_digit((string)this->value)
        {
            throw this->getException(Asrt::INVALID_DIGIT, message, "Value '%s' is not a digit.", fieldName);
        }

        return this;
    }


    public function date(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->notEmpty(message, fieldName);
        if unlikely is_object(this->value) && get_class(this->value) === "\DateTime"
        {
            return this;
        }
        if unlikely strtotime(this->value) === false
        {
            throw this->getException(Asrt::INVALID_DATE, message, "Value '%s' is not a date.");
        }

        return this;
    }


    public function dateRange(string dateMin, string dateMax, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->date(message, fieldName);
        (new Asrt(dateMin))->date("Invalid input date for dateMin");
        (new Asrt(dateMax))->date("Invalid input date for dateMax");
        var date            = "";
        if this->value instanceof DateTime
        {
            let date            = this->value->format("U");
        }
        else
        {
            let date            = strtotime(this->value);
        }
        var min                 = strtotime(dateMin);
        var max                 = strtotime(dateMax);
        if unlikely date < min || date > max
        {
            throw this->getException(Asrt::INVALID_DATE_RANGE, message, "Value %s is not between " . dateMin . " and " . dateMax . ".", fieldName);
        }

        return this;
    }


    public function after(afterDate, string message="", string fieldName="")
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->notEmpty(message, fieldName);
        if unlikely ( strtotime(this->value) === false && strtotime(afterDate) === false && strtotime(this->value) < strtotime(afterDate) )
        {
             throw this->getException(Asrt::INVALID_DATE, message, "Value '%s' is not a date, comparison date is not a date or value is not before comparison date.", fieldName);
        }

        return this;
    }



    public function integerish(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely is_object(this->value) || strval(intval(this->value)) != this->value || is_bool(this->value) || is_null(this->value)
        {
            throw this->getException(Asrt::INVALID_INTEGERISH, message, "Value '%s' is not an integer or a number castable to integer.", fieldName);
        }

        return this;
    }



    public function isBool(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ( ! is_bool(this->value) )
        {
            throw this->getException(Asrt::INVALID_BOOLEAN, message, "Value '%s' is not a boolean.", fieldName);
        }

        return this;
    }


    public function scalar(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_scalar(this->value)
        {
            throw this->getException(Asrt::INVALID_SCALAR, message, "Value '%s' is not a scalar.", fieldName);
        }

        return this;
    }


    public function notEmpty(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ( is_object(this->value) && empty (array)this->value ) || empty this->value
        {
            throw this->getException(Asrt::VALUE_EMPTY, message, "Value '%s' is empty, but non empty value was expected.", fieldName);
        }

        return this;
    }


    public function noContent(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! empty this->value
        {
            throw this->getException(Asrt::VALUE_NOT_EMPTY, message, "Value '%s' is not empty, but empty value was expected.", fieldName);
        }

        return this;
    }


    public function notNull(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value === null
        {
            throw this->getException(Asrt::VALUE_NULL, message, "Value '%s' is null, but non null value was expected.", fieldName);
        }

        return this;
    }


    public function isString(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_string(this->value)
        {
            throw this->getException(Asrt::INVALID_STRING, message, "Value '%s' expected to be string, type " . gettype(this->value) . " given.", fieldName);
        }

        return this;
    }


    public function regex(string pattern, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! this->runRegex(pattern, fieldName)
        {
            throw this->getException(Asrt::INVALID_REGEX, message, "Value '%s' does not match expression.", fieldName, ["pattern": pattern]);
        }

        return this;
    }


    protected function runRegex(string pattern, string fieldName="") -> bool
    {
        var result;
        try
        {
            let result              = preg_match(pattern, this->value);
        }
        catch Exception
        {

        }
        if ( result === false )
        {
            throw this->getException(Asrt::INVALID_NOT_VALID_REGEX, "", "Value '" . pattern . "' does not valid regular expression.", fieldName, ["pattern": pattern]);
        }

        return (bool)result;
    }


    public function notRegex(string pattern, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely this->runRegex(pattern, fieldName)
        {
            throw this->getException(Asrt::INVALID_NOT_REGEX, message, "Value '%s' does not match expression.", fieldName, ["pattern": pattern]);
        }

        return this;
    }


    public function length(int length, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ( mb_strlen(this->value, encoding) !== length )
        {
            throw this->getException(Asrt::INVALID_LENGTH, message, "Value '%s' has to be " . length . " exactly characters long, but length is " . mb_strlen(this->value, encoding) . ".", fieldName, ["length": length, "encoding": encoding]);
        }

        return this;
    }


    public function minLength(int minLength, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely mb_strlen(this->value, encoding) < minLength
        {
            throw this->getException(Asrt::INVALID_MIN_LENGTH, message, "Value '%s' is too short, it should have more than " . minLength . " characters, but only has " . mb_strlen(this->value, encoding) . " characters.", fieldName, ["min_length": minLength, "encoding": encoding]);
        }

        return this;
    }


    public function maxLength(int maxLength, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely mb_strlen(this->value, encoding) > maxLength
        {
            throw this->getException(Asrt::INVALID_MAX_LENGTH, message,"Value '%s' is too long, it should have no more than ". maxLength ." characters, but has " . mb_strlen(this->value, encoding) . " characters.", fieldName, ["max_length": maxLength, "encoding": encoding]);
        }

        return this;
    }


    public function betweenLength(int minLength, int maxLength, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        this->minLength(minLength, message, fieldName, encoding);
        this->maxLength(maxLength, message, fieldName, encoding);

        return this;
    }


    public function startsWith(string needle, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely mb_strpos(this->value, needle, 0, encoding) !== 0
        {
            throw this->getException(Asrt::INVALID_STRING_START, message, "Value '%s' does not start with '" . this->stringify(needle) . "'.", fieldName, ["needle": needle, "encoding": encoding]);
        }

        return this;
    }



    public function endsWith(string needle, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        var stringPosition      = 0;
        let stringPosition      = mb_strlen(this->value, encoding) - mb_strlen(needle, encoding);
        if unlikely mb_strripos(this->value, needle, 0 , encoding) !== stringPosition
        {
            throw this->getException(Asrt::INVALID_STRING_END, message, "Value '%s' does not end with '" . needle . "'.", fieldName, ["needle": needle, "encoding": encoding]);
        }

        return this;
    }


    public function contains(string needle, string message="", string fieldName="", string encoding="utf8") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely mb_strpos(this->value, needle, 0, encoding) === false
        {
            throw this->getException(Asrt::INVALID_STRING_CONTAINS, message, "Value '%s' does not contain '" . needle . "'.", fieldName, ["needle": needle, "encoding": encoding]);
        }

        return this;
    }


    public function choice(array choices, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! in_array(this->value, choices, true)
        {
            var idx                     = 0;
            var choice                  = "";
            for idx, choice in choices
            {
                let choices[idx]            = this->stringify(choice);
            }
            throw this->getException(Asrt::INVALID_CHOICE, message, "Value '%s' is not an element of the valid values: '" . implode(", ", choices) . ".", fieldName, ["choices": choices]);
        }

        return this;
    }


    public function inArray(array choices, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->choice(choices, message, fieldName);

        return this;
    }



    public function numeric(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_numeric(this->value)
        {
            throw this->getException(Asrt::INVALID_NUMERIC, message, "Value '%s' is not numeric.", fieldName);
        }

        return this;
    }


    public function nonEmptyArray(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isArray(message, fieldName);
        if unlikely empty this->value
        {
            throw this->getException(Asrt::INVALID_NOT_BLANK, message, "Value '%s' is not a non-empty array.", fieldName);
        }

        return this;
    }


    public function nonEmptyInt(string message="", string fieldName="") -> <Asrt>
    {
        var msg                 = message;
        let msg                 = msg ? msg : "Value '%s' is not a non-empty integer.";

        return this->isInt(message, fieldName)->notEmpty(message, fieldName);
    }


    public function nonEmptyString(string message="", string fieldName="") -> <Asrt>
    {
        var msg                 = message;
        let msg                 = msg ? msg : "Value '%s' is not a non-empty string.";

        return this->isString(message, fieldName)->notEmpty(message, fieldName);
    }


    public function isArray(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_array(this->value)
        {
            throw this->getException(Asrt::INVALID_ARRAY, message, "Value '%s' is not an array.", fieldName);
        }

        return this;
    }


    public function isTraversable(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_array(this->value) && ! is_object(this->value) && ! ( this->value instanceof Traversable )
        {
            throw this->getException(Asrt::INVALID_TRAVERSABLE, message, "Value '%s' is not an array and does not implement Traversable.", fieldName);
        }

        return this;
    }

    public function isArrayAccessible(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_array(this->value) && ! ( this->value instanceof ArrayAccess )
        {
            throw this->getException(Asrt::INVALID_ARRAY_ACCESSIBLE, message, "Value '%s' is not an array and does not implement ArrayAccess.", fieldName);
        }

        return this;
    }


    public function keyExists(var key, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isArray(message, fieldName);
        if unlikely ! array_key_exists(key, this->value)
        {
            throw this->getException(Asrt::INVALID_KEY_EXISTS, message, "Array does not contain an element with key '%s'", fieldName, ["key": key]);
        }

        return this;
    }


    public function keysExist(array keys, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isArray(message, fieldName);
        var key;
        for key in keys
        {
            if unlikely ! array_key_exists(key, this->value)
            {
                throw this->getException(Asrt::INVALID_KEYS_EXIST, message, "Array does not contain an element with key '%s'", fieldName, ["key": key]);
            }
        }

        return this;
    }


    public function propertyExists(key, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isObject(message, fieldName);
        if unlikely ! property_exists(this->value, key) && !isset( this->value->{key} )
        {
            throw this->getException(Asrt::INVALID_PROPERTY_EXISTS, message, "Object does not contain a property with key '%s'", fieldName, ["key": key]);
        }

        return this;
    }


    public function propertiesExist(array keys, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isObject(message, fieldName);
        var key;
        for key in keys
        {
            // Using isset to allow resolution of magically defined properties
            if unlikely ! property_exists(this->value, key) && !isset( this->value->{key} )
            {
                throw this->getException(Asrt::INVALID_PROPERTIES_EXIST, message, "Object does not contain a property with key '%s'", fieldName, ["key": key]);
            }
        }

        return this;
    }


    public function utf8(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely mb_detect_encoding(this->value, "UTF-8", true) !== "UTF-8"
        {
            throw this->getException(Asrt::INVALID_UTF8, message, "Value '%s' was expected to be a valid UTF8 string", fieldName);
        }

        return this;
    }


    public function ascii(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! this->runRegex("/^[ -~]+/", fieldName)
        {
            throw this->getException(Asrt::INVALID_ASCII, message, "Value '%s' was expected to be a valid ASCII string", fieldName);
        }

        return this;
    }


    public function keyIsset(key, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isArrayAccessible(message, fieldName);
        if unlikely ! isset( this->value[key] )
        {
            throw this->getException(Asrt::INVALID_KEY_ISSET, message, "The element with key '%s' was not found", fieldName, ["key": key]);
        }

        return this;
    }


    public function notEmptyKey(key, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->keyIsset(key, message, fieldName);
        (new Asrt(this->value[key]))->setExceptionClass(this->exceptionClass)->notEmpty(message, fieldName);

        return this;
    }


    public function notBlank(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely false === this->value || empty this->value  && "0" != this->value
        {
            throw this->getException(Asrt::INVALID_NOT_BLANK, message, "Value '%s' is blank, but was expected to contain a value.", fieldName);
        }

        return this;
    }


    public function isInstanceOf(string className, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! ( this->value instanceof className )
        {
            throw this->getException(Asrt::INVALID_INSTANCE_OF, message, "Class '%s' was expected to be instanceof of '" . className . "' but is not.", fieldName, ["class": className]);
        }

        return this;
    }


    public function notIsInstanceOf(string className, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value instanceof className
        {
            throw this->getException(Asrt::INVALID_NOT_INSTANCE_OF, message, "Class '%s' was not expected to be instanceof of '" . className . "'.", fieldName, ["class": className]);
        }

        return this;
    }


    public function subclassOf(string className, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_subclass_of(this->value, className)
        {
            throw this->getException(Asrt::INVALID_SUBCLASS_OF, message, "Class '%s' was expected to be subclass of '" . className . "'.", fieldName, ["class": className]);
        }

        return this;
    }


    public function range(float minValue, float maxValue, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->numeric(message, fieldName);
        if unlikely this->value < minValue || this->value > maxValue
        {
            throw this->getException(Asrt::INVALID_RANGE, message, "Number '%s' was expected to be at least '" . this->stringify(minValue) . "' and at most '" . this->stringify(maxValue) . "'.", fieldName, ["min": minValue, "max": maxValue]);
        }

        return this;
    }


    public function min(int minValue, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->numeric(message, fieldName);
        if unlikely this->value < minValue
        {
            throw this->getException(Asrt::INVALID_MIN, message, "Number '%s' was expected to be at least '" . this->stringify(minValue) . "'.", fieldName, ["min": minValue]);
        }

        return this;
    }


    public function max(int maxValue, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->numeric(message, fieldName);
        if unlikely this->value > maxValue
        {
            throw this->getException(Asrt::INVALID_MAX, message, "Number '%s' was expected to be at most '" . this->stringify(maxValue) . "'.", fieldName, ["max": maxValue]);
        }

        return this;
    }


    public function file(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        this->notEmpty(message, fieldName);
        if unlikely ! is_file(this->value)
        {
            throw this->getException(Asrt::INVALID_FILE, message, "File '%s' was expected to exist.", fieldName);
        }

        return this;
    }


    public function fileOrDirectoryExists(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        this->notEmpty(message, fieldName);
        if unlikely ! file_exists(this->value)
        {
            throw this->getException(Asrt::INVALID_FILE_OR_DIR, message, "File or directory '%s' was expected to exist.", fieldName);
        }

        return this;
    }


    public function directory(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! is_dir(this->value)
        {
            throw this->getException(Asrt::INVALID_DIRECTORY, message, "Path '%s' was expected to be a directory.", fieldName);
        }

        return this;
    }


    public function readable(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! is_readable(this->value)
        {
            throw this->getException(Asrt::INVALID_READABLE, message, "Path '%s' was expected to be readable.", fieldName);
        }

        return this;
    }


    public function writeable(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! is_writeable(this->value)
        {
            throw this->getException(Asrt::INVALID_WRITEABLE, message, "Path '%s' was expected to be writeable.", fieldName);
        }

        return this;
    }


    public function email(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! filter_var(this->value, FILTER_VALIDATE_EMAIL)
        {
            throw this->getException(Asrt::INVALID_EMAIL, message, "Value '%s' was expected to be a valid e-mail address.", fieldName);
        }

        return this;
    }


    public function emailPrefix(string message="", string fieldName="") -> <Asrt>
    {
        this->value(this->value . "@example.com");

        return this->email(message, fieldName);
    }


    public function url(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! filter_var(this->value, FILTER_VALIDATE_URL)
        {
            throw this->getException(Asrt::INVALID_URL, message, "Value '%s' was expected to be a valid URL starting with http or https", fieldName);
        }

        return this;
    }


    public function ipAddress(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        var pattern   = "/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])/";
        if unlikely ! this->runRegex(pattern, fieldName)
        {
            throw this->getException(Asrt::INVALID_IP_ADDRESS, message, "Value '%s' was expected to be a valid IP Address", fieldName);
        }

        return this;
    }


    public function domainName(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! filter_var(this->value, FILTER_VALIDATE_DOMAIN)
        {
            throw this->getException(Asrt::INVALID_DOMAIN_NAME, message, "Value '%s' was expected to be a valid domain name", fieldName);
        }

        return this;
    }


    public function ausMobile(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! static::isAusMobile(this->value)
        {
            throw this->getException(Asrt::INVALID_AUS_MOBILE, message, "Value '%s' is not an australian mobile number.", fieldName);
        }

        return this;
    }

    public static function isAusMobile(string value) -> bool
    {
        let value               = trim(str_replace(" ", "", value));
        let value               = preg_replace("|^" . chr(92) . "+61|", "0", value);

        return (bool)preg_match("/^04[0-9]{8}/", value);
    }


    public function alnum(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! this->runRegex("/(^([a-zA-Z]{1}[a-zA-Z0-9]*))/", fieldName)
        {
            throw this->getException(Asrt::INVALID_ALNUM, message, "Value '%s' is not alphanumeric, starting with letters and containing only letters and numbers.", fieldName);
        }

        return this;
    }


    public function isTrue(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value !== true
        {
            throw this->getException(Asrt::INVALID_TRUE, message, "Value '%s' is not TRUE.", fieldName);
        }

        return this;
    }


    public function truthy(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! this->value
        {
            throw this->getException(Asrt::INVALID_TRUE, message, "Value '%s' is not truthy.", fieldName);
        }

        return this;
    }


    public function isFalse(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value !== false
        {
            throw this->getException(Asrt::INVALID_FALSE, message, "Value '%s' is not FALSE.", fieldName);
        }

        return this;
    }

    public function notFalse(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely this->value === false
        {
            throw this->getException(Asrt::INVALID_NOT_FALSE, message, "Value '%s' is not FALSE.", fieldName);
        }

        return this;
    }


    public function classExists(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! class_exists(this->value)
        {
            throw this->getException(Asrt::INVALID_CLASS, message, "Class '%s' does not exist.", fieldName);
        }

        return this;
    }


    public function implementsInterface(string interfaceName, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        var reflection;
        let reflection          = new ReflectionClass(this->value);
        if unlikely ! reflection->implementsInterface(interfaceName)
        {
            throw this->getException(Asrt::INTERFACE_NOT_IMPLEMENTED, message, "Class '%s' does not implement interface '" . this->stringify(interfaceName) . "'.", fieldName, ["interface": interfaceName]);
        }

        return this;
    }


    public function isJsonString(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if null === json_decode(this->value) && JSON_ERROR_NONE !== json_last_error()
        {
            throw this->getException(Asrt::INVALID_JSON_STRING, message, "Value '%s' is not a valid JSON string.", fieldName);
        }

        return this;
    }

    /**
     * Assert that value is a valid UUID.
     *
     * Uses code from {@link https://github.com/ramsey/uuid} that is MIT licensed.
     *
     * @param string message
     * @param string fieldName
     * @return Assert
     * @throws AssertionFailedException
     */
    public function uuid(string message="", string fieldName="") -> <Asrt>
    {
        if unlikely this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        let this->value         = str_replace(["urn:", "uuid:", "{", "}"], "", this->value);
        if unlikely this->value === "00000000-0000-0000-0000-000000000000"
        {
            return this;
        }
        if unlikely ! this->runRegex("/^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}/", fieldName)
        {
            throw this->getException(Asrt::INVALID_UUID, message, "Value '%s' is not a valid UUID.", fieldName);
        }

        return this;
    }

    /**
     * Assert that value is a valid samAccountName (in line with Active
     * directory sAMAccountName restrictions for users).
     *
     * From: @link https://social.technet.microsoft.com/wiki/contents/articles/11216.active-directory-requirements-for-creating-objects.aspx#Objects_with_sAMAccountName_Attribute
     *      The schema allows 256 characters in sAMAccountName values. However, the system limits sAMAccountName to
     *      20 characters for user objects and 16 characters for computer objects. The following characters are not
     *      allowed in sAMAccountName values: " [ ] : ; | = + * ? < > / \ ,
     *      You cannot logon to a domain using a sAMAccountName that includes the "@" character. If a user has a
     *      sAMAccountName with this character, they must logon using their userPrincipalName (UPN).
     *
     * @param string message
     * @param string fieldName
     * @return Assert
     * @throws AssertionFailedException
     */
    public function samAccountName(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! this->runRegex("/^[a-z0-9]{4,20}$/", fieldName)
        {
            throw this->getException(Asrt::INVALID_SAMACCOUNTNAME, message, "Value '%s' is not a valid samAccountName.", fieldName);
        }

        return this;
    }


    public function userPrincipalName(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_string(this->value) || ! filter_var(this->value, FILTER_VALIDATE_EMAIL)
        {
            throw this->getException(Asrt::INVALID_USERPRINCIPALNAME, message, "Value '%s' is not a valid userPrincipalName.", fieldName);
        }

        return this;
    }


    public function unc(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        // #^\\\\\\\\[a-zA-Z0-9\.\-_]{1,}(\\\\[a-zA-Z0-9\-_]{1,}){1,}[\]{0,1}#
        this->isString(message, fieldName);
        if unlikely ! this->runRegex("=^\\\\\\\\[a-zA-Z0-9.-]+(\\\\[a-zA-Z0-9`~!@#$%^&(){}\'._-]+([ ]+[a-zA-Z0-9`~!@#$%^&(){}\'._-]+)*)+$=s", fieldName)
        {
            throw this->getException(Asrt::INVALID_UNC_PATH, message, "Value '%s' is not a valid UNC path.", fieldName);
        }

        return this;
    }


    public function driveLetter(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->isString(message, fieldName);
        if unlikely ! this->runRegex("/^[a-zA-Z]:/", fieldName)
        {
            throw this->getException(Asrt::INVALID_DRIVE_LETTER, message, "Value '%s' is not a valid drive letter.", fieldName);
        }

        return this;
    }


    public function isni(string message="", string fieldName="")
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! this->runRegex("/^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9X]{1}/", fieldName)
        {
            throw this->getException(Asrt::INVALID_ISNI, message, "Value '%s' is not a valid ISNI.", fieldName);
        }

        return this;
    }


    public function count(int count, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely count !== count(this->value)
        {
            throw this->getException(Asrt::INVALID_COUNT, message, "List does not contain exactly '%d' elements.", fieldName, ["count": count]);
        }

        return this;
    }


    protected function doAllOrNullOr(func, args) -> boolean
    {
        if unlikely this->nullOr && is_null(this->value)
        {
            return true;
        }
        if this->emptyOr && empty(this->value)
        {
            return true;
        }
        if unlikely this->all
        {
            this->getCopy()->isTraversable();
            var values              = this->value;
            if is_object(this->value) && this->value instanceof ArrayObject
            {
                let values              = values->getArrayCopy();
            }
            var value;
            for value in values
            {
                var obj                 = this->getCopy()->value(value);
                call_user_func_array([obj, func], args);
            }

            return true;
        }
        if unlikely this->nullOr && is_null(this->value)
        {
            return true;
        }
        if this->emptyOr && empty(this->value)
        {
            return true;
        }

        return false;
    }


    public function choicesNotEmpty(array choices, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        this->notEmpty(message, fieldName);
        var choice;
        for choice in choices
        {
            this->notEmptyKey(choice, message, fieldName);
        }

        return this;
    }


    public function methodExists(var obj, string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        Asrt::that(obj)->setExceptionClass(this->exceptionClass)->isObject(message, fieldName);
        if unlikely ! method_exists(obj, this->value)
        {
            throw this->getException(Asrt::INVALID_METHOD, message, "Expected '%s' does not a exist in provided object.", fieldName);
        }

        return this;
    }


    public function isObject(string message="", string fieldName="") -> <Asrt>
    {
        if this->doAllOrNullOr(__FUNCTION__, func_get_args())
        {
            return this;
        }
        if unlikely ! is_object(this->value)
        {
            throw this->getException(Asrt::INVALID_OBJECT, message, "Provided '%s' is not a valid object.", fieldName);
        }

        return this;
    }


    private function stringify(var value) -> string
    {
        if unlikely typeof value === "boolean"
        {
            return value ? "<TRUE>" : "<FALSE>";
        }
        if typeof value === "integer" || typeof value === "float" || typeof value === "string"
        {
            var val             = "";
            if typeof value !== "string"
            {
                let val             = (string)value;
            }
            if strlen(val) > 100
            {
                let val             = substr(val, 0, 97) . "...";
            }

            return val;
        }
        if typeof value === "array"
        {
            return "<ARRAY>";
        }
        if typeof value === "object"
        {
            return get_class(value);
        }
        if is_resource(value)
        {
            return "<RESOURCE>";
        }
        if value === null
        {
            return "<NULL>";
        }

        return "unknown";
    }

    public function getCopy() -> <Asrt>
    {
        return (new Asrt(this->value))
                    ->setExceptionClass(this->getExceptionClass(""))
                    ->fieldName(this->getFieldName(""))
                    ->code(this->getCode(0))
                    ->error(this->getError(""));
    }
}

