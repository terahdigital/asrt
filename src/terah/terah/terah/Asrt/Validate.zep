namespace Terah\Asrt;


class Validate extends Asrt
{
    public static function that(var value, string fieldName="", int code=406, string error="", string level="warning") -> <Asrt>
    {
        var asrt;
        let asrt                = new Asrt(value, fieldName, code, error, level);
        asrt->setExceptionClass("ValidationFailedException");

        return asrt;
    }
}