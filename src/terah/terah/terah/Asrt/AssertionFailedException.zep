namespace Terah\Asrt;

use stdClass;
use Terah\Utils\Str;

/**
 * Assert
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to terry@terah.com.au so I can send you a copy immediately.
 */

/**
 * AssertionFailedException
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 * @author Terry Cullen <terry@terah.com.au>
 *
 */
class AssertionFailedException extends \Exception
{
    private fieldName;
    private value;
    private constraints;
    private level;
    private propertyPath;
    private location;

    public function __construct(string message, int code, string fieldName="", value="", array constraints=[], string level="critical", string propertyPath="")
    {
        parent::__construct(message, code);
        let this->fieldName     = fieldName;
        let this->value         = value;
        let this->constraints   = constraints;
        let this->level         = level;
        let this->propertyPath  = propertyPath;

        var stackTrace          = this->getTrace();
        var idx                 = 0;
        var point               = [];
        for idx, point in stackTrace {

            if idx > 500
            {
                break;
            }
            var className           = "";
            if ! empty point["class"] {

                let className           = point["class"];
            }
            if ( strpos("Asrt", className) === false )
            {
                var lastLine;
                let lastLine            = idx - 1;
                if array_key_exists(lastLine, stackTrace)
                {
                    let this->location  = (object)stackTrace[idx - 1];
                }
                else
                {
                    let this->location  = (object)point;
                }

                break;
            }
        }
    }

    /**
     * Get the field name that was set for the assertion object.
     *
     * @return string
     */
    public function getFieldName() -> string
    {
        return this->fieldName;
    }

    /**
     * Get the value that caused the assertion to fail.
     *
     * @return mixed
     */
    public function getValue()
    {
        return this->value;
    }

    /**
     * Get the constraints that applied to the failed assertion.
     *
     * @return array
     */
    public function getConstraints() -> array
    {
        return this->constraints;
    }

    /**
     * Get the error level.
     *
     * @return string
     */
    public function getLevel() -> string
    {
        if this->level {

            return this->level;
        }

        return "critical";
    }

    /**
     * User controlled way to define a sub-property causing
     * the failure of a currently asserted objects.
     *
     * Useful to transport information about the nature of the error
     * back to higher layers.
     *
     * @return string
     */
    public function getPropertyPath() -> string
    {
        if this->propertyPath {

            return this->propertyPath;
        }

        return "General Error";
    }

    /**
     * Get the propertyPath, combined with the calling file and line location.
     *
     * @return string
     */
    public function getPropertyPathAndCallingLocation() -> string
    {
        return this->getPropertyPath() . " in " . this->getCallingFileAndLine();
    }

    /**
     * Get the calling file and line from where the failing assertion
     * was called.
     *
     * @return string
     */
    protected function getCallingFileAndLine() -> string
    {
        var stackTrace          = this->getTrace();
        var trace               = [];
        for trace in stackTrace {

            let trace               = (object)trace;
            if ( empty trace->file )
            {
                continue;
            }
            var file                = Str::beforeLast(".php", Str::afterLast("/", trace->file));
            if ( in_array(file, ["AssertionChain", "Assertion"]) )
            {
                continue;
            }

            return sprintf("%s:%d", trace->file, trace->line);
        }

        return "";
    }

    /**
     * Get the trace location of where the failing assertion
     * was called.
     *
     * @return object
     */
    public function getLocation() -> <stdClass>
    {
        if ( ! this->location )
        {
            return new stdClass();
        }

        return this->location;
    }
}