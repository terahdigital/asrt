namespace Terah\Tester;

use Closure;
use Terah\Asrt\Asrt;

class Test
{
    /** @var string  */
    public testName         = "";

    /** @var string  */
    public successMsg       = "";

    /** @var Closure  */
    public test             = null;

    /** @var string */
    public exceptionType    = null;

    /** @var int */
    public exceptionCode    = null;

    /** @var string */
    public exceptionMsg     = null;


    public function __construct(string testName, <Closure> test, string successMsg="", int exceptionCode=0, string exceptionClass="", string exceptionMsg="")
    {
        this->setTestName(testName);
        this->setTest(test);
        this->setSuccessMessage(successMsg);
        this->setExceptionCode(exceptionCode);
        this->setExceptionType(exceptionClass);
        this->setExceptionMsg(exceptionMsg);
    }


    public function getTestName() -> string
    {
        return this->testName;
    }


    public function setTestName(string testName) -> <Test>
    {
        Asrt::that(testName)->notEmpty();

        let this->testName      = testName;

        return this;
    }


    public function getSuccessMessage() -> string
    {
        if ( ! this->successMsg )
        {
            return sprintf("Successfully run %s", this->testName);
        }

        return this->successMsg;
    }


    public function setSuccessMessage(string successMsg) -> <Test>
    {
        let this->successMsg    = successMsg;

        return this;
    }


    public function getTest() -> <Closure>
    {
        return this->test;
    }


    public function setTest(<Closure> test) -> <Test>
    {
        let this->test          = test;

        return this;
    }


    public function getExceptionType() -> string
    {
        return this->exceptionType;
    }


    public function setExceptionType(string exceptionType) -> <Test>
    {
        let this->exceptionType = exceptionType;

        return this;
    }


    public function getExceptionMsg() -> string
    {
        return this->exceptionMsg;
    }


    public function setExceptionMsg(string exceptionMsg) -> <Test>
    {
        let this->exceptionMsg = exceptionMsg;

        return this;
    }


    public function getExceptionCode() -> int
    {
        return this->exceptionCode;
    }


    public function setExceptionCode(int exceptionCode) -> <Test>
    {
        let this->exceptionCode = exceptionCode;

        return this;
    }


    public function runTest(<Suite> suite)
    {
        return this->getTest()->__invoke(suite);
    }

}