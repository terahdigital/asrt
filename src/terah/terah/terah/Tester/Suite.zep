namespace Terah\Tester;

use Closure;
use Throwable;
use Psr\Log\LoggerInterface as Logger;
use Terah\Asrt\Asrt;

class Suite
{
    /** @var Closure[] */
    protected suiteUps         = [];

    /** @var Closure[] */
    protected setUps           = [];

    /** @var Closure[] */
    protected tearDowns        = [];

    /** @var Closure[] */
    protected suiteDowns       = [];

    /** @var Test[] */
    protected tests            = [];

    /** @var Closure[] */
    protected fixtureClosures  = [];

    /** @var mixed[] */
    protected fixtures         = [];

    /** @var Logger */
    protected logger           = null;

    /** @var int **/
    protected failedCount      = 0;

    /** @var int */
    protected runCount         = 0;


    public function __construct(<LoggerInterface> logger) 
    {
        let this->logger        = logger;
    }


    public function getStats() -> string
    {
        var out                 = sprintf("Num tests: %d", count(this->tests));

        return out;
    }


    public function run(string filter="") -> int
    {
        var filter              = preg_quote(filter, "/");
        var func;
        this->logger->debug(sprintf("Running %d suiteUps", count(this->suiteUps)));
        for func in this->suiteUps
        {
            func->__invoke(this);
        }
        this->logger->debug(sprintf("Running test %d cases with filter %s", count(this->tests), filter));
        var testCase;
        for testCase in this->tests
        {
            this->runTest(testCase, filter);
        }
        this->logger->debug(sprintf("Running %d suiteDowns", count(this->suiteDowns)));
        for func in this->suiteDowns
        {
            func->__invoke(this);
        }

        return this->failedTestsCount();
    }


    protected function runTest(<Test> testCase, string filter) -> bool
    {
        var testName            = testCase->getTestName();
        var func;
        var e;
        this->logger->info(sprintf("[%s] - Testing filer (%s)", testName, filter));
        if filter && ! preg_match(sprintf("/%s/", filter), testName)
        {
            return true;
        }
        let this->runCount++;
        try
        {
            this->logger->info(sprintf("[%s] - Starting...", testName));
            for func in this->setUps
            {
                func->__invoke(this);
            }
            testCase->runTest(this);
            if ( testCase->exceptionType )
            {
                throw new \Exception(sprintf("The test was suppose to throw a %s exception but didn't", testCase->exceptionType));
            }
            for func in this->tearDowns
            {
                func->__invoke(this);
            }
            this->logger->info(sprintf("[%s] - %d", testName, testCase->getSuccessMessage()));
        }
        catch Throwable, e
        {
            var expectedCode        = testCase->getExceptionCode();
            var expectedClass       = testCase->getExceptionType();
            var expectedMsg         = testCase->getExceptionMsg();
            var code                = e->getCode();
            var message             = e->getMessage();
            var exceptionClass      = get_class(e);
            var context             = [["testName" : testName], e];
            if ( ! expectedClass &&  ! expectedCode )
            {
                this->logger->error(e->getMessage(), context);
                let this->failedCount++;

                return false;
            }
            if ( expectedCode && expectedCode !== code )
            {
                this->logger->error(sprintf("Exception code(%d) was expected to be (%d)", code, expectedCode), context);
                let this->failedCount++;

                return false;
            }
            if ( expectedMsg && expectedMsg !== message )
            {
                this->logger->error(sprintf("Exception message(%d) was expected to be (%s)", message, expectedMsg), context);
                let this->failedCount++;

                return false;
            }
            if ( expectedClass && expectedClass !== exceptionClass )
            {
                this->logger->error(sprintf("Exception class(%s) was expected to be (%s)", exceptionClass, expectedClass), context);
                let this->failedCount++;

                return false;
            }
            this->logger->info(sprintf("[%s] - %s", testName, testCase->getSuccessMessage()));
        }

        return true;
    }


    public function totalTestsRunCount() -> int
    {
        return this->runCount;
    }


    public function failedTestsCount() -> int
    {
        return this->failedCount;
    }


    public function onSuiteStart(<Closure> callback) -> <Suite>
    {
        this->logger->debug("Adding suiteUp");
        let this->suiteUps[]    = callback;

        return this;
    }


    public function onSuiteFinish(<Closure> callback) -> <Suite>
    {
        this->logger->debug("Adding suiteDown");
        let this->suiteDowns[]  = callback;

        return this;
    }


    public function setUp(<Closure> callback) -> <Suite>
    {
        this->logger->debug("Adding setUp");
        let this->setUps[]      = callback;

        return this;
    }


    public function tearDown(<Closure> callback) -> <Suite>
    {
        this->logger->debug("Adding tearDown");
        let this->tearDowns[]   = callback;

        return this;
    }


    public function test(string testName, <Closure> test, string successMessage="", int exceptionCode=0, string exceptionClass="", string exceptionMsg="") -> <Suite>
    {
        this->logger->debug("Adding test");
        var testcase;
        let testcase            = new Test(testName, test, successMessage, exceptionCode, exceptionClass, exceptionMsg);
        let this->tests[]       = testcase;
        this->logger->debug(sprintf("There are now %d tests now", count(this->tests)));

        return this;
    }


    public function fixture(string fixtureName, <Closure> value) -> <Suite>
    {
        this->logger->debug("Adding fixture");
        let this->fixtureClosures[fixtureName] = value;

        return this;
    }


    public function getFixture(string fixtureName, bool forceReload=false)
    {
        Asrt::that(this->fixtureClosures)->keyExists(fixtureName, sprintf("The fixture (%s) does not exist.", fixtureName));

        if ( forceReload || ! array_key_exists(fixtureName, this->fixtures) && is_callable(this->fixtureClosures[fixtureName]) )
        {
            let this->fixtures[fixtureName] = this->fixtureClosures[fixtureName]->__invoke(this);
        }
        Asrt::that(this->fixtures)->keyExists(fixtureName, sprintf("The fixture with the key (%s) does not exist.", fixtureName));

        return this->fixtures[fixtureName];
    }


    public function getLogger() -> <Logger>
    {
        return this->logger;
    }
}