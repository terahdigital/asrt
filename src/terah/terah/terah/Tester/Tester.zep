namespace Terah\Tester;

use Psr\Log\LoggerInterface;
use Terah\Asrt\Asrt;
use Terah\Logger\Logger;

class Tester
{
    /** @var Suite[]  */
    protected suites            = [];

    /** @var LoggerInterface logger */
    protected logger;


    public function runTests(string suiteSources, string suite, string testFilter, bool recursive, string level=Logger::INFO) -> void
    {
        let this->logger        = new Logger("php://stdin", level);
        var totalFailed         = 0;
        var totalTests          = 0;
        var testFiles           = this->getTestFiles(suiteSources, recursive);

        if empty testFiles
        {
            this->logger->error("No test files found/specified");

            exit(1);
        }
        var fileName;
        for fileName in testFiles
        {
            this->logger->debug("Loading test file " . fileName);
            require(fileName);
        }
        var suiteName;
        for suiteName, _ in this->suites
        {
            this->logger->debug("Processing suite: " . suiteName);
            if ( suite && suiteName !== suite )
            {
                this->logger->debug("Skipping suite: " . suiteName);

                continue;
            }
            this->logger->debug("Running suite: " . suiteName);
            var results             = this->run(suiteName, testFilter);
            let totalFailed         = totalFailed + results["totalFailed"];
            let totalTests          = totalTests + results["totalTests"];
        }
        if totalFailed > 0
        {
            this->logger->error(sprintf("Tests failed - %d of %d tests have failed.", totalFailed, totalTests));

            exit(1);
        }
        this->logger->info(sprintf("Tests succeeded - %d tests have passed.", totalTests));

        exit(0);
    }


    public function suite(string name) -> <Suite>
    {
        Asrt::that(name)->notEmpty();
        Asrt::that(array_key_exists(name, this->suites))->isFalse("The test with the name (" . name . ") already exists.");
        var suite;
        let suite               = new Suite(this->logger);
        let this->suites[name]  = suite;

        return suite;
    }


    public function run(string name, string testFilter="") -> array
    {
        Asrt::that(this->suites)->keyExists(name, "The test suite (" . name . ") has not been loaded");
        var totalFailed         = 0;
        var totalTests          = 0;
        var suite;
        this->logger->debug("Running " . count(this->suites) . " suites");
        for suite in this->suites
        {
            this->logger->debug(suite->getStats());
            let totalFailed         = totalFailed + suite->run(testFilter);
            let totalTests          = totalTests + suite->totalTestsRunCount();
        }

        return ["totalFailed" : totalFailed, "totalTests" : totalTests];
    }


    protected function getTestFiles(string fileName="", bool recursive=false) -> array
    {
        if empty fileName
        {
            return [];
        }
        if ! file_exists(fileName)
        {
            this->logger->error(fileName . " does not exist; exiting");

            exit(1);
        }
        let fileName            = realpath(fileName);
        if is_dir(fileName)
        {
            var iterator;
            let iterator            = new \DirectoryIterator(fileName);
            if ( recursive )
            {
                let iterator            = new \RecursiveDirectoryIterator(fileName);
            }
            var testFiles           = [];
            var fileInfo;
            for fileInfo in iterator
            {
                if preg_match("/Suite.php/", fileInfo->getBasename())
                {
                    let testFiles[]         = fileInfo->getPathname();
                }
            }

            return testFiles;
        }
        if ! is_file(fileName)
        {
            this->logger->error(fileName . " is not a file; exiting");

            exit(1);
        }

        return [fileName];
    }
}