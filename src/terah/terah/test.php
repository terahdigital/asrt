<?php

use Terah\Logger\Logger;

class TestLogger extends Logger
{

    public static function toRed(string $strText) : string
    {
        return "\e[0;31m" . $strText . "\e[0m";
    }


    public static function fixToRed(string $strText)
    {
        $data           = parent::toRed($strText);

        return str_replace("\\e", "\e", $data);
    }

    protected function writeToFile(string $content)
    {
        gzwrite($this->handle, str_replace("\\e", "\e", $content));
    }

}


echo Logger::toRed('Logger::toRed with a \ or \\ ') . PHP_EOL;
echo TestLogger::toRed('TestLogger::toRed with a \ or \\ ') . PHP_EOL;
echo TestLogger::toRed('TestLogger::fixToRed with a \ or \\ ') . PHP_EOL;


(new TestLogger('php://stdout'))->info('test');