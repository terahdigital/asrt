<?php

namespace Terah\Logger;

use Closure;
use Throwable;
use Exception;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;

class Logger implements \Psr\Log\LoggerInterface
{

    const EMERGENCY = 'emergency';


    const ALERT = 'alert';


    const CRITICAL = 'critical';


    const ERROR = 'error';


    const WARNING = 'warning';


    const NOTICE = 'notice';


    const INFO = 'info';


    const DEBUG = 'debug';


    const BLACK = 'black';


    const DARK_GRAY = 'dark_gray';


    const BLUE = 'blue';


    const LIGHT_BLUE = 'light_blue';


    const GREEN = 'green';


    const LIGHT_GREEN = 'light_green';


    const CYAN = 'cyan';


    const LIGHT_CYAN = 'light_cyan';


    const RED = 'red';


    const LIGHT_RED = 'light_red';


    const PURPLE = 'purple';


    const LIGHT_PURPLE = 'light_purple';


    const BROWN = 'brown';


    const YELLOW = 'yellow';


    const MAGENTA = 'magenta';


    const LIGHT_GRAY = 'light_gray';


    const WHITE = 'white';


    const DEF = 'default';


    const BOLD = 'bold';


    const IDX_LEVEL = 0;


    const IDX_FORE = 1;


    const IDX_BACK = 2;


    const IDX_FONT = 3;


    const IDX_LABEL = 4;

    /**
     * @var resource handle The file handle
     */
    protected $handle = null;

    /**
     * @var string level
     */
    protected $level = self::INFO;

    /**
     * @var bool closeLocally
     */
    protected $closeLocally = false;

    /**
     * @var bool
     */
    protected $addDate = true;

    /**
     * @var string
     */
    protected $separator = ' | ';

    /**
     * @var \Closure
     */
    protected $formatter = null;

    /**
     * @var string
     */
    protected $lastLogEntry = '';

    /**
     * @var bool|null
     */
    protected $gzipFile = null;

    /**
     * @var bool
     */
    protected $useLocking = false;


    static protected $logLevels = array('emergency' => array(1, self::WHITE, self::RED, self::DEF, 'EMERG'), 'alert' => array(2, self::WHITE, self::YELLOW, self::DEF, 'ALERT'), 'critical' => array(3, self::RED, self::DEF, self::BOLD, 'CRIT'), 'error' => array(4, self::RED, self::DEF, self::DEF, 'ERROR'), 'warning' => array(5, self::YELLOW, self::DEF, self::DEF, 'WARN'), 'notice' => array(6, self::CYAN, self::DEF, self::DEF, 'NOTE'), 'info' => array(7, self::GREEN, self::DEF, self::DEF, 'INFO'), 'debug' => array(8, self::LIGHT_GRAY, self::DEF, self::DEF, 'DEBUG'));


    static protected $colours = array('fore' => array('black' => '0;30', 'dark_gray' => '1;30', 'blue' => '0;34', 'light_blue' => '1;34', 'green' => '0;32', 'light_green' => '1;32', 'cyan' => '0;36', 'light_cyan' => '1;36', 'red' => '0;31', 'light_red' => '1;31', 'purple' => '0;35', 'light_purple' => '1;35', 'brown' => '0;33', 'yellow' => '1;33', 'magenta' => '0;35', 'light_gray' => '0;37', 'white' => '1;37'), 'back' => array('default' => '49', 'black' => '40', 'red' => '41', 'green' => '42', 'yellow' => '43', 'blue' => '44', 'magenta' => '45', 'cyan' => '46', 'light_gray' => '47'), 'bold' => array());


    /**
     * @param mixed $handle
     * @param string $level
     * @param bool $useLocking
     * @param bool $gzipFile
     * @param bool $addDate
     */
    public function __construct($handle = null, string $level = self::INFO, bool $useLocking = false, bool $gzipFile = false, bool $addDate = true)
    {
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     */
    public function emergency($message, array $context = array())
    {
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     */
    public function alert($message, array $context = array())
    {
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     */
    public function critical($message, array $context = array())
    {
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     */
    public function error($message, array $context = array())
    {
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     */
    public function warning($message, array $context = array())
    {
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     */
    public function notice($message, array $context = array())
    {
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     */
    public function info($message, array $context = array())
    {
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     */
    public function debug($message, array $context = array())
    {
    }

    /**
     * @param string $handle
     * @return Logger
     */
    public function setLogFile(string $handle): Logger
    {
    }

    /**
     * @param string $str
     * @param string $foregroundColor
     * @param string $backgroundColor
     * @param bool $bold
     * @return string
     */
    public static function addColour(string $str, string $foregroundColor = '', string $backgroundColor = '', bool $bold = false): string
    {
    }

    /**
     * @param string $str
     * @param string $foregroundColor
     * @param string $backgroundColor
     * @param bool $bold
     * @return string
     */
    public function colourize(string $str, string $foregroundColor = '', string $backgroundColor = '', bool $bold = false): string
    {
    }

    /**
     * @param string $level Ignore logging attempts at a level less the level
     * @return Logger
     */
    public function setLogLevel(string $level): Logger
    {
    }

    /**
     * @return Logger
     */
    public function lock(): Logger
    {
    }

    /**
     * @return Logger
     */
    public function gzipped(): Logger
    {
    }

    /**
     * @param callable $fnFormatter
     * @return Logger
     */
    public function formatter($fnFormatter): Logger
    {
    }

    /**
     * Log messages to handle
     *
     * @param mixed $level The level of the log message
     * @param string $message If an object is passed it must implement __toString()
     * @param array $context Placeholders to be substituted in the message
     */
    public function log($level, $message, array $context = array())
    {
    }

    /**
     * @param string $level
     * @param string $message
     * @return string
     */
    public static function style(string $level, string $message): string
    {
    }

    /**
     * @param string $level
     * @param string $message
     * @param array $context
     * @return string
     */
    protected function formatMessage(string $level, string $message, array $context = array()): string
    {
    }

    /**
     * @param string $content
     */
    public function write(string $content)
    {
    }


    protected function loadResource()
    {
    }

    /**
     * @return string
     */
    public function getLastLogEntry(): string
    {
    }


    public function __destruct()
    {
    }

    /**
     * @param string $level
     * @return array
     */
    protected static function getLevelMap(string $level): array
    {
    }

}
