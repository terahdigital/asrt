<?php

namespace Terah\Asrt;

/**
 * ValidationFailedException
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 * @author Terry Cullen <terry@terah.com.au>
 *
 */
class ValidationFailedException extends \Terah\Asrt\AssertionFailedException
{

}
