<?php

namespace Terah\Asrt;

use Closure;

class Test
{
    /**
     * @var string
     */
    public $testName = '';

    /**
     * @var string
     */
    public $successMessage = '';

    /**
     * @var Closure
     */
    public $test = null;

    /**
     * @var string
     */
    public $exceptionType = null;

    /**
     * @var int
     */
    public $exceptionCode = null;

    /**
     * @var string
     */
    public $exceptionMsg = null;


    /**
     * @param string $testName
     * @param \Closure $test
     * @param string $successMessage
     * @param int $exceptionCode
     * @param string $exceptionClass
     * @param string $exceptionMsg
     */
    public function __construct(string $testName, \Closure $test, string $successMessage = '', int $exceptionCode = 0, string $exceptionClass = '', string $exceptionMsg = '')
    {
    }

    /**
     * @return string
     */
    public function getTestName(): string
    {
    }

    /**
     * @param string $testName
     * @return Test
     */
    public function setTestName(string $testName): Test
    {
    }

    /**
     * @return string
     */
    public function getSuccessMessage(): string
    {
    }

    /**
     * @param string $successMessage
     * @return Test
     */
    public function setSuccessMessage(string $successMessage): Test
    {
    }

    /**
     * @return \Closure
     */
    public function getTest(): Closure
    {
    }

    /**
     * @param \Closure $test
     * @return Test
     */
    public function setTest(\Closure $test): Test
    {
    }

    /**
     * @return string
     */
    public function getExceptionType(): string
    {
    }

    /**
     * @param string $exceptionType
     * @return Test
     */
    public function setExceptionType(string $exceptionType): Test
    {
    }

    /**
     * @return string
     */
    public function getExceptionMsg(): string
    {
    }

    /**
     * @param string $exceptionMsg
     * @return Test
     */
    public function setExceptionMsg(string $exceptionMsg): Test
    {
    }

    /**
     * @return int
     */
    public function getExceptionCode(): int
    {
    }

    /**
     * @param int $exceptionCode
     * @return Test
     */
    public function setExceptionCode(int $exceptionCode): Test
    {
    }

    /**
     * @param Suite $suite
     */
    public function runTest(Suite $suite)
    {
    }

}
