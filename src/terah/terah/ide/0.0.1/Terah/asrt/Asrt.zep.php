<?php

namespace Terah\Asrt;

use DateTime;
use ReflectionClass;
use Traversable;
use ArrayAccess;

class Asrt
{

    const ASSERTION_EXCEPTION = 'AssertionFailedException';


    const VALIDATION_EXCEPTION = 'ValidationFailedException';


    const INVALID_FLOAT = 1;


    const INVALID_INTEGER = 2;


    const INVALID_DIGIT = 3;


    const INVALID_INTEGERISH = 4;


    const INVALID_BOOLEAN = 5;


    const VALUE_EMPTY = 6;


    const VALUE_NULL = 7;


    const INVALID_STRING = 8;


    const INVALID_REGEX = 9;


    const INVALID_MIN_LENGTH = 10;


    const INVALID_MAX_LENGTH = 11;


    const INVALID_STRING_START = 12;


    const INVALID_STRING_CONTAINS = 13;


    const INVALID_CHOICE = 14;


    const INVALID_NUMERIC = 15;


    const INVALID_ARRAY = 16;


    const INVALID_KEY_EXISTS = 17;


    const INVALID_NOT_BLANK = 18;


    const INVALID_INSTANCE_OF = 19;


    const INVALID_SUBCLASS_OF = 20;


    const INVALID_RANGE = 21;


    const INVALID_ALNUM = 22;


    const INVALID_TRUE = 23;


    const INVALID_EQ = 24;


    const INVALID_SAME = 25;


    const INVALID_MIN = 26;


    const INVALID_MAX = 27;


    const INVALID_LENGTH = 28;


    const INVALID_FALSE = 29;


    const INVALID_STRING_END = 30;


    const INVALID_UUID = 31;


    const INVALID_COUNT = 32;


    const INVALID_NOT_EQ = 33;


    const INVALID_NOT_SAME = 34;


    const INVALID_TRAVERSABLE = 35;


    const INVALID_ARRAY_ACCESSIBLE = 36;


    const INVALID_KEY_ISSET = 37;


    const INVALID_SAMACCOUNTNAME = 38;


    const INVALID_USERPRINCIPALNAME = 39;


    const INVALID_DIRECTORY = 40;


    const INVALID_FILE = 41;


    const INVALID_READABLE = 42;


    const INVALID_WRITEABLE = 43;


    const INVALID_CLASS = 44;


    const INVALID_EMAIL = 45;


    const INTERFACE_NOT_IMPLEMENTED = 46;


    const INVALID_URL = 47;


    const INVALID_NOT_INSTANCE_OF = 48;


    const VALUE_NOT_EMPTY = 49;


    const INVALID_JSON_STRING = 50;


    const INVALID_OBJECT = 51;


    const INVALID_METHOD = 52;


    const INVALID_SCALAR = 53;


    const INVALID_DATE = 54;


    const INVALID_CALLABLE = 55;


    const INVALID_KEYS_EXIST = 56;


    const INVALID_PROPERTY_EXISTS = 57;


    const INVALID_PROPERTIES_EXIST = 58;


    const INVALID_UTF8 = 59;


    const INVALID_DOMAIN_NAME = 60;


    const INVALID_NOT_FALSE = 61;


    const INVALID_FILE_OR_DIR = 62;


    const INVALID_ASCII = 63;


    const INVALID_NOT_REGEX = 64;


    const INVALID_GREATER_THAN = 65;


    const INVALID_LESS_THAN = 66;


    const INVALID_GREATER_THAN_OR_EQ = 67;


    const INVALID_LESS_THAN_OR_EQ = 68;


    const INVALID_IP_ADDRESS = 69;


    const INVALID_AUS_MOBILE = 70;


    const INVALID_ISNI = 71;


    const INVALID_DATE_RANGE = 72;


    const INVALID_UNC_PATH = 73;


    const INVALID_DRIVE_LETTER = 74;


    const INVALID_NOT_VALID_REGEX = 75;


    const EMERGENCY = 'emergency';


    const ALERT = 'alert';


    const CRITICAL = 'critical';


    const ERROR = 'error';


    const WARNING = 'warning';


    const NOTICE = 'notice';


    const INFO = 'info';


    const DEBUG = 'debug';

    /**
     * @var bool
     */
    protected $nullOr = false;

    /**
     * @var bool
     */
    protected $emptyOr = false;

    /**
     * @var mixed
     */
    protected $value = null;

    /**
     * @var bool
     */
    protected $all = false;

    /**
     * @var string
     */
    protected $fieldName = '';

    /**
     * @var string
     */
    protected $propertyPath = '';

    /**
     * @var string
     */
    protected $level = 'critical';

    /**
     * @var int
     */
    protected $overrideCode = null;

    /**
     * @var string
     */
    protected $overrideError = '';

    /**
     * Exception to throw when an assertion failed.
     *
     * @var string
     */
    protected $exceptionClass = '';


    /**
     * @param mixed $value
     * @param string $fieldName
     * @param int $code
     * @param string $error
     * @param string $level
     */
    public function __construct($value, string $fieldName = '', int $code = 0, string $error = '', string $level = 'warning')
    {
    }

    /**
     * @param mixed $value
     * @param string $fieldName
     * @param int $code
     * @param string $error
     * @param string $level
     * @return Asrt
     */
    public static function that($value, string $fieldName = '', int $code = 0, string $error = '', string $level = 'warning'): Asrt
    {
    }

    /**
     * @param \Closure[] validators
     * @return array
     * @param array $validators
     * @param \Closure  [] validators
     */
    public static function runValidators(array $validators): array
    {
    }

    /**
     * @param mixed $value
     * @return Asrt
     */
    public function reset($value): Asrt
    {
    }

    /**
     * @param mixed $value
     * @return Asrt
     */
    public function value($value): Asrt
    {
    }

    /**
     * @param bool $nullOr
     * @return Asrt
     */
    public function nullOr(bool $nullOr = true): Asrt
    {
    }

    /**
     * @param bool $emptyOr
     * @return Asrt
     */
    public function emptyOr(bool $emptyOr = true): Asrt
    {
    }

    /**
     * @param bool $all
     * @return Asrt
     */
    public function all(bool $all = true): Asrt
    {
    }

    /**
     * @param int $code
     * @param string $message
     * @param string $defaultMessage
     * @param string $fieldName
     * @param array $constraints
     * @param string $level
     * @return AssertionFailedException
     */
    public function getException(int $code, string $message, string $defaultMessage, string $fieldName = '', array $constraints = array(), string $level = ''): AssertionFailedException
    {
    }

    /**
     * @param string $exceptionClass
     * @return Asrt
     */
    public function setExceptionClass(string $exceptionClass): Asrt
    {
    }

    /**
     * @param string $def
     * @return string
     */
    public function getExceptionClass(string $def = ''): string
    {
    }

    /**
     * @param int $code
     * @return Asrt
     */
    public function code(int $code): Asrt
    {
    }

    /**
     * @param int $def
     * @return int
     */
    public function getCode(int $def): int
    {
    }

    /**
     * @param string $fieldName
     * @return Asrt
     */
    public function fieldName(string $fieldName): Asrt
    {
    }

    /**
     * @param string $def
     * @return string
     */
    public function getFieldName(string $def): string
    {
    }

    /**
     * @param string $fieldName
     * @return Asrt
     */
    public function name(string $fieldName): Asrt
    {
    }

    /**
     * @param string $level
     * @return Asrt
     */
    public function level(string $level): Asrt
    {
    }

    /**
     * @param string $def
     * @return string
     */
    public function getLevel(string $def): string
    {
    }

    /**
     * @param string $error
     * @return Asrt
     */
    public function error(string $error): Asrt
    {
    }

    /**
     * @param string $def
     * @return string
     */
    public function getError(string $def): string
    {
    }

    /**
     * @param string $propertyPath
     * @return Asrt
     */
    public function propertyPath(string $propertyPath): Asrt
    {
    }

    /**
     * @param string $def
     * @return string
     */
    public function getPropertyPath(string $def): string
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function eq($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function greaterThan($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function greaterThanOrEq($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function lessThan($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function lessThanOrEq($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function same($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notEq($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isCallable(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value2
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notSame($value2, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function id(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function unsignedInt(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function flag(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function status(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function nullOrId(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function allIds(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isInt(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isFloat(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function digit(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function date(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $dateMin
     * @param string $dateMax
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function dateRange(string $dateMin, string $dateMax, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $afterDate
     * @param string $message
     * @param string $fieldName
     */
    public function after($afterDate, string $message = '', string $fieldName = '')
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function integerish(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isBool(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function scalar(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notEmpty(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function noContent(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notNull(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isString(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $pattern
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function regex(string $pattern, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $pattern
     * @param string $fieldName
     * @return bool
     */
    protected function runRegex(string $pattern, string $fieldName = ''): bool
    {
    }

    /**
     * @param string $pattern
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notRegex(string $pattern, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param int $length
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function length(int $length, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param int $minLength
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function minLength(int $minLength, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param int $maxLength
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function maxLength(int $maxLength, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param int $minLength
     * @param int $maxLength
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function betweenLength(int $minLength, int $maxLength, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param string $needle
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function startsWith(string $needle, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param string $needle
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function endsWith(string $needle, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param string $needle
     * @param string $message
     * @param string $fieldName
     * @param string $encoding
     * @return Asrt
     */
    public function contains(string $needle, string $message = '', string $fieldName = '', string $encoding = 'utf8'): Asrt
    {
    }

    /**
     * @param array $choices
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function choice(array $choices, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param array $choices
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function inArray(array $choices, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function numeric(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function nonEmptyArray(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function nonEmptyInt(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function nonEmptyString(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isArray(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isTraversable(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isArrayAccessible(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $key
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function keyExists($key, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param array $keys
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function keysExist(array $keys, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $key
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function propertyExists($key, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param array $keys
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function propertiesExist(array $keys, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function utf8(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function ascii(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $key
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function keyIsset($key, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $key
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notEmptyKey($key, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notBlank(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $className
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isInstanceOf(string $className, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $className
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notIsInstanceOf(string $className, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $className
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function subclassOf(string $className, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param double $minValue
     * @param double $maxValue
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function range(float $minValue, float $maxValue, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param int $minValue
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function min(int $minValue, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param int $maxValue
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function max(int $maxValue, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function file(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function fileOrDirectoryExists(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function directory(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function readable(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function writeable(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function email(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function emailPrefix(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function url(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function ipAddress(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function domainName(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function ausMobile(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $value
     * @return bool
     */
    public static function isAusMobile(string $value): bool
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function alnum(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isTrue(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function truthy(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isFalse(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function notFalse(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function classExists(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $interfaceName
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function implementsInterface(string $interfaceName, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isJsonString(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * Assert that value is a valid UUID.
     *
     * Uses code from {@link https://github.com/ramsey/uuid} that is MIT licensed.
     *
     * @param string $message
     * @param string $fieldName
     * @return Assert
     * @throws AssertionFailedException
     */
    public function uuid(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * Assert that value is a valid samAccountName (in line with Active
     * directory sAMAccountName restrictions for users).
     *
     * From: @link https://social.technet.microsoft.com/wiki/contents/articles/11216.active-directory-requirements-for-creating-objects.aspx#Objects_with_sAMAccountName_Attribute
     *      The schema allows 256 characters in sAMAccountName values. However, the system limits sAMAccountName to
     *      20 characters for user objects and 16 characters for computer objects. The following characters are not
     *      allowed in sAMAccountName values: " [ ] : ; | = + ? < > / \ ,
     *      You cannot logon to a domain using a sAMAccountName that includes the "@" character. If a user has a
     *      sAMAccountName with this character, they must logon using their userPrincipalName (UPN).
     *
     * @param string $message
     * @param string $fieldName
     * @return Assert
     * @throws AssertionFailedException
     */
    public function samAccountName(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function userPrincipalName(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function unc(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function driveLetter(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     */
    public function isni(string $message = '', string $fieldName = '')
    {
    }

    /**
     * @param int $count
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function count(int $count, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $func
     * @param mixed $args
     * @return bool
     */
    protected function doAllOrNullOr($func, $args): bool
    {
    }

    /**
     * @param array $choices
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function choicesNotEmpty(array $choices, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $obj
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function methodExists($obj, string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param string $message
     * @param string $fieldName
     * @return Asrt
     */
    public function isObject(string $message = '', string $fieldName = ''): Asrt
    {
    }

    /**
     * @param mixed $value
     * @return string
     */
    private function stringify($value): string
    {
    }

    /**
     * @return Asrt
     */
    public function getCopy(): Asrt
    {
    }

}
