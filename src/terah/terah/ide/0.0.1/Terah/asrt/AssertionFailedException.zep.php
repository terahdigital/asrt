<?php

namespace Terah\Asrt;

use stdClass;
use Terah\Utils\Str;

/**
 * AssertionFailedException
 *
 * @author Benjamin Eberlei <kontakt@beberlei.de>
 * @author Terry Cullen <terry@terah.com.au>
 *
 */
class AssertionFailedException extends \Exception
{

    private $fieldName;


    private $value;


    private $constraints;


    private $level;


    private $propertyPath;


    private $location;


    /**
     * @param string $message
     * @param int $code
     * @param string $fieldName
     * @param mixed $value
     * @param array $constraints
     * @param string $level
     * @param string $propertyPath
     */
    public function __construct(string $message, int $code, string $fieldName = '', $value = '', array $constraints = array(), string $level = 'critical', string $propertyPath = '')
    {
    }

    /**
     * Get the field name that was set for the assertion object.
     *
     * @return string
     */
    public function getFieldName(): string
    {
    }

    /**
     * Get the value that caused the assertion to fail.
     *
     * @return mixed
     */
    public function getValue()
    {
    }

    /**
     * Get the constraints that applied to the failed assertion.
     *
     * @return array
     */
    public function getConstraints(): array
    {
    }

    /**
     * Get the error level.
     *
     * @return string
     */
    public function getLevel(): string
    {
    }

    /**
     * User controlled way to define a sub-property causing
     * the failure of a currently asserted objects.
     *
     * Useful to transport information about the nature of the error
     * back to higher layers.
     *
     * @return string
     */
    public function getPropertyPath(): string
    {
    }

    /**
     * Get the propertyPath, combined with the calling file and line location.
     *
     * @return string
     */
    public function getPropertyPathAndCallingLocation(): string
    {
    }

    /**
     * Get the calling file and line from where the failing assertion
     * was called.
     *
     * @return string
     */
    protected function getCallingFileAndLine(): string
    {
    }

    /**
     * Get the trace location of where the failing assertion
     * was called.
     *
     * @return object
     */
    public function getLocation(): stdClass
    {
    }

}
