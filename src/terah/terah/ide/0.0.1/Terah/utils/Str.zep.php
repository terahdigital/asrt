<?php

namespace Terah\Utils;

class Str
{

    /**
     * @param string $start
     * @param string $end
     * @param string $hayStack
     * @return string
     */
    public static function between(string $start, string $end, string $hayStack): string
    {
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function before(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists = false): string
    {
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function after(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists = false): string
    {
    }

    /**
     * @param string $start
     * @param string $end
     * @param string $hayStack
     * @return string
     */
    public static function betweenLast(string $start, string $end, string $hayStack): string
    {
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    public static function afterLast(string $needle, string $hayStack, bool $returnOrigIfNeedleNotExists = false): string
    {
    }

    /**
     * @param string $str
     * @param string $needle
     * @return int
     */
    public static function strrevpos(string $str, string $needle): int
    {
    }

    /**
     * @param string $needle
     * @param string $hayStack
     * @return string
     */
    public static function beforeLast(string $needle, string $hayStack): string
    {
    }

    /**
     * @param string $hayStack
     * @param bool $returnOrigIfNeedleNotExists
     * @return string
     */
    protected static function emptyOrHaystack(string $hayStack, bool $returnOrigIfNeedleNotExists): string
    {
    }

}
