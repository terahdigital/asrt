<?php

namespace Terah\Tester;

use Closure;
use Terah\Asrt\Asrt;

class Test
{
    /**
     * @var string
     */
    public $testName = '';

    /**
     * @var string
     */
    public $successMsg = '';

    /**
     * @var Closure
     */
    public $test = null;

    /**
     * @var string
     */
    public $exceptionType = null;

    /**
     * @var int
     */
    public $exceptionCode = null;

    /**
     * @var string
     */
    public $exceptionMsg = null;


    /**
     * @param string $testName
     * @param \Closure $test
     * @param string $successMsg
     * @param int $exceptionCode
     * @param string $exceptionClass
     * @param string $exceptionMsg
     */
    public function __construct(string $testName, \Closure $test, string $successMsg = '', int $exceptionCode = 0, string $exceptionClass = '', string $exceptionMsg = '')
    {
    }

    /**
     * @return string
     */
    public function getTestName(): string
    {
    }

    /**
     * @param string $testName
     * @return Test
     */
    public function setTestName(string $testName): Test
    {
    }

    /**
     * @return string
     */
    public function getSuccessMessage(): string
    {
    }

    /**
     * @param string $successMsg
     * @return Test
     */
    public function setSuccessMessage(string $successMsg): Test
    {
    }

    /**
     * @return \Closure
     */
    public function getTest(): Closure
    {
    }

    /**
     * @param \Closure $test
     * @return Test
     */
    public function setTest(\Closure $test): Test
    {
    }

    /**
     * @return string
     */
    public function getExceptionType(): string
    {
    }

    /**
     * @param string $exceptionType
     * @return Test
     */
    public function setExceptionType(string $exceptionType): Test
    {
    }

    /**
     * @return string
     */
    public function getExceptionMsg(): string
    {
    }

    /**
     * @param string $exceptionMsg
     * @return Test
     */
    public function setExceptionMsg(string $exceptionMsg): Test
    {
    }

    /**
     * @return int
     */
    public function getExceptionCode(): int
    {
    }

    /**
     * @param int $exceptionCode
     * @return Test
     */
    public function setExceptionCode(int $exceptionCode): Test
    {
    }

    /**
     * @param Suite $suite
     */
    public function runTest(Suite $suite)
    {
    }

}
