<?php

namespace Terah\Tester;

use Closure;
use Throwable;
use Psr\Log\LoggerInterface;
use Terah\Asrt\Asrt;

class Suite
{
    /**
     * @var Closure[]
     */
    protected $suiteUps = array();

    /**
     * @var Closure[]
     */
    protected $setUps = array();

    /**
     * @var Closure[]
     */
    protected $tearDowns = array();

    /**
     * @var Closure[]
     */
    protected $suiteDowns = array();

    /**
     * @var Test[]
     */
    protected $tests = array();

    /**
     * @var Closure[]
     */
    protected $fixtureClosures = array();

    /**
     * @var mixed[]
     */
    protected $fixtures = array();

    /**
     * @var Logger
     */
    protected $logger = null;

    /**
     * @var int
     */
    protected $failedCount = 0;

    /**
     * @var int
     */
    protected $runCount = 0;


    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
    }

    /**
     * @return string
     */
    public function getStats(): string
    {
    }

    /**
     * @param string $filter
     * @return int
     */
    public function run(string $filter = ''): int
    {
    }

    /**
     * @param Test $testCase
     * @param string $filter
     * @return bool
     */
    protected function runTest(Test $testCase, string $filter): bool
    {
    }

    /**
     * @return int
     */
    public function totalTestsRunCount(): int
    {
    }

    /**
     * @return int
     */
    public function failedTestsCount(): int
    {
    }

    /**
     * @param \Closure $callback
     * @return Suite
     */
    public function onSuiteStart(\Closure $callback): Suite
    {
    }

    /**
     * @param \Closure $callback
     * @return Suite
     */
    public function onSuiteFinish(\Closure $callback): Suite
    {
    }

    /**
     * @param \Closure $callback
     * @return Suite
     */
    public function setUp(\Closure $callback): Suite
    {
    }

    /**
     * @param \Closure $callback
     * @return Suite
     */
    public function tearDown(\Closure $callback): Suite
    {
    }

    /**
     * @param string $testName
     * @param \Closure $test
     * @param string $successMessage
     * @param int $exceptionCode
     * @param string $exceptionClass
     * @param string $exceptionMsg
     * @return Suite
     */
    public function test(string $testName, \Closure $test, string $successMessage = '', int $exceptionCode = 0, string $exceptionClass = '', string $exceptionMsg = ''): Suite
    {
    }

    /**
     * @param string $fixtureName
     * @param \Closure $value
     * @return Suite
     */
    public function fixture(string $fixtureName, \Closure $value): Suite
    {
    }

    /**
     * @param string $fixtureName
     * @param bool $forceReload
     */
    public function getFixture(string $fixtureName, bool $forceReload = false)
    {
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger(): Logger
    {
    }

}
