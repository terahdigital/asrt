<?php

namespace Terah\Tester;

use Psr\Log\LoggerInterface;
use Terah\Asrt\Asrt;
use Terah\Logger\Logger;

class Tester
{
    /**
     * @var Suite[]
     */
    protected $suites = array();

    /**
     * @var LoggerInterface logger
     */
    protected $logger;


    /**
     * @param string $suiteSources
     * @param string $suite
     * @param string $testFilter
     * @param bool $recursive
     * @param string $level
     */
    public function runTests(string $suiteSources, string $suite, string $testFilter, bool $recursive, string $level = Logger::INFO)
    {
    }

    /**
     * @param string $name
     * @return Suite
     */
    public function suite(string $name): Suite
    {
    }

    /**
     * @param string $name
     * @param string $testFilter
     * @return array
     */
    public function run(string $name, string $testFilter = ''): array
    {
    }

    /**
     * @param string $fileName
     * @param bool $recursive
     * @return array
     */
    protected function getTestFiles(string $fileName = '', bool $recursive = false): array
    {
    }

}
